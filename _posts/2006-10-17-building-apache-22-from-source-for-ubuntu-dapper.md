---
layout: post
title: 'Building Apache 2.2 from source for Ubuntu Dapper'
tags:
  - apache
  - dapper
  - howto
  - server
  - source
  - ubuntu

---

Two reasons you might want to do this.

1. You want to host a Rails application using Mongrel via Apache and `mod_proxy_balancer`.
2. You're studying in a Website administration module for your 3rd year Software Engineering degree :)

Seriously though, if you don't want to use `mod_proxy_balancer`, just do a normal `apt-get install` of Apache 2 and you'll be fine. `mod_proxy_balancer` is only available for Apache 2.2, and currently, that's not available from the Ubuntu repositories via `apt-get`.

This article only covers installing Apache 2.2 - I'll write another one for getting Subversion and PHP working shortly afterwards.

### Workspace

If you've not got the `build-essential` package installed yet:

    sudo apt-get install build-essential

It's best to keep all of the source files in a seperate directory so they don't mess up your home directory.

    cd
    mkdir src
    cd src

### Zlib

So that Apache can compress output to browsers that support it, we're going to install Zlib first of all:

    wget http://www.zlib.net/zlib-1.2.3.tar.gz
    tar xvfz zlib-1.2.3.tar.gz
    cd zlib-1.2.3/
    ./configure --prefix=/usr/local
    make
    sudo make install

### Apache 2.2

Now download the Apache 2.2 source files:

    cd ..
    wget http://apache.rmplc.co.uk/httpd/httpd-2.2.3.tar.gz

Extract and move into the directory:

    tar xvfz httpd-2.2.3.tar.gz
    cd httpd-2.2.3/

Now to configure the build of Apache 2.2 that we want:

    ./configure --prefix=/usr/local/apache2 --enable-mods-shared=all --enable-deflate --enable-proxy --enable-proxy-balancer --enable-proxy-http

Besides setting the modules we'd like installed, and the location of the install, this paramater `--enable-mods-shared=all` is telling Apache 2.2 to build modules so that they can be dynamically loaded when it is started. This means, we can add further modules to our Apache 2.2 install when we like - as we will do with the Subversion modules and PHP.

Once the configuration is complete:

    make
    sudo make install

Let's test that it's working:

    sudo /usr/local/apache2/bin/apachectl start

Now navigate to [http://localhost](http://localhost) and you should see a message saying "It works!".

Stop Apache:

    sudo /usr/local/apache2/bin/apachectl stop

### Apache at start-up

Now let's get Apache to start at boot time automatically:

    sudo cp /usr/local/apache2/bin/apachectl /etc/init.d/apachectl
    sudo chmod +x /etc/init.d/apachectl

What we're doing here is copying the Apache Control script into the start-up directory.

We just need to add a few lines to the file for it to work nicely:

    sudo nano /etc/init.d/apachectl

Add the followinig, so the top of the file looks like:

    #!/bin/sh
    #
    # chkconfig: - 85 15
    # description: Apache is a web server.

Save the file. 

Now we need to register it with the start-up manager:

    sudo /usr/sbin/update-rc.d apachectl defaults

### Securing Apache

It's also a good idea to create a dedicate Apache system user account. It'll make your install much more secure.

    sudo adduser --system apache

Now we just need to make sure that Apache runs under this user. We do that by editting the configuration file:

    sudo nano /usr/local/apache2/conf/httpd.conf

You need to find the lines that say:

    User daemon
    Group daemon

And change them so they look like:

    User apache
    Group nogroup

Save the file.

Now, let's start Apache:

    sudo /usr/local/apache2/bin/apachectl start

Now to check it's running under the new user, `apache`:

    ps -aux | grep httpd

If you see the word `apache` in there, it's working.

### Check it's all working

Now just reboot the system and before logging in, check on another machine by visiting the servers IP in the web browser and you should see the "It works!" message. This means Apache started up correctly automatically.

Building Apache 2.2 from source. Done.
