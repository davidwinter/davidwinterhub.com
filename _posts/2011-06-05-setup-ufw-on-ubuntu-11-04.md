---
layout: post
title: 'Setup UFW on Ubuntu 11.04'
tags:
  - security
  - server
  - ubuntu
  - ufw

---

Not much more than a few hours have passed since I posted my [APF setup howto](/articles/2011/06/05/install-apf-on-ubuntu-11-04/), but I've found something even simpler.

`ufw` - uncomplicated firewall. It does exactly as it says on the tin.

    sudo aptitude install ufw
    sudo ufw allow 22
    sudo ufw enable

That's ssh sorted, and the firewall is enabled. It's important to allow port 22 first before enabling, otherwise you'll get locked out. You can see the status of the firewall by running:

    sudo ufw status verbose

And to allow Apache traffic:

    sudo ufw allow 80

That's it. Outgoing traffic is allowed by default. Everything else is blocked. Also, all of these rules are saved transparently, so you don't have to worry about things like that. Reboot away, and everything will just work.

If you need to disable `ufw` just run:

    sudo ufw disable
