---
layout: post
title: 'Install PHP5, lighttpd and Imagick on Mac OS X Leopard'
tags:
  - howto
  - imagick
  - lighttpd
  - mac
  - php

---

It's a little tricky getting a nice clean install of [PHP5](http://www.php.net) for OS X 10.5. The packages that I always used to depend on over at [Entropy](http://www.entropy.ch/software/macosx/php/) don't seem to work anymore. At least not for Leopard. A great shame.

I'm now switching to [lighttpd](http://www.lighttpd.net) for my development server, so have come up with the following recipe to install PHP5 and the great [Imagick](http://www.php.net/imagick) extension for image manipulation.

Follow these instructions at your own risk! They worked fine on my setup but I can't guarantee they will for you.

1. Install [Xcode](http://developer.apple.com/technology/xcode.html).
2. Install [MacPorts](http://www.macports.org/).
3. Now update MacPorts:
    
        sudo port selfupdate
    
4. Install lighttpd:
    
        sudo port install lighttpd
    
    Install the startup item and copy the default configuration file:
    
        sudo launchctl load -w /Library/LaunchDaemons/org.macports.lighttpd.plist
        sudo cp /opt/local/etc/lighttpd/lighttpd.conf.default /opt/local/etc/lighttpd/lighttpd.conf
    
7. Now we install PHP with the FastCGI, MySQL and Pear variants.
    
        sudo port install php5 +fastcgi +mysql5 +pear
    
    In order for this to work, I had to disable the port `perl5.8` by using the following command:
    
        sudo port deactivate perl5.8
        sudo port activate perl5
        
    Then run the command again and it should succeed:
    
        sudo port install php5 +fastcgi +mysql5 +pear
    
8. Copy over the default PHP configuration:
    
        sudo cp /opt/local/etc/php.ini-dist /opt/local/etc/php.ini
    
9. Now open up the PHP configuration file:
    
        sudo mate /opt/local/etc/php.ini
    
    Add the following line to the file:
    
        cgi.fix_pathinfo=1
    
    Find `extension_dir = "./"` and replace it with:
    
        extension_dir = "/opt/local/lib/php/extensions"
        extension=imagick.so
    
13. Edit the lighttpd configuration file:
    
        sudo mate /opt/local/etc/lighttpd/lighttpd.conf
    
    Ensure that the following is uncommented:
    
        fastcgi.server = ( ".php" =>
            ( "localhost" =>
                (
                    "socket" => "/var/run/lighttpd/php-fastcgi.socket",
                    "bin-path" => "/opt/local/bin/php-cgi"
                )
            )
        )
    
    And also make lighttpd run as the user `www`:
    
        server.username = "www"
        server.groupname = "www"
    
    Add the following if you want to be able to use your `Sites` directory in your home directory. I also changed the default document root to the OS X default:
    
        server.modules += ("mod_userdir")
        userdir.path = "Sites"
        userdir.basepath = "/Users/"
        server.document-root = "/Library/WebServer/Documents"
    
    Ensure that `"mod_fastcgi",` is uncommented.
    
17. Now we need to setup a few directories, and make the user `www` the owner for them:
    
        sudo mkdir /var/run/lighttpd
        sudo mkdir /var/log/lighttpd
        sudo chown www:www /var/run/lighttpd
        sudo chown www:www /var/log/lighttpd
    
20. Now install ImageMagick:
    
        sudo port install imagemagick
    
21. Now we need to download the Imagick PHP module. I was hoping this would be as easy as `sudo pecl install imagick`, however I got an error for that, and so a manual build and install of the module is necessary:
    
        curl -O http://pecl.php.net/get/imagick-2.2.1.tgz
        tar xvfz imagick-2.2.1.tgz
        cd imagick-2.2.1/
        phpize
        ./configure --with-imagick=/opt/local
        make
        sudo make install
        sudo cp /opt/local/lib/php/extensions/no-debug-non-zts-20060613/imagick.so /opt/local/lib/php/extensions

And finally, you can then run LightTPD with the following. The `-D` flag tells lighttpd not to run in the background--you don't need to use it:

    lighttpd -f /opt/local/etc/lighttpd/lighttpd.conf -D

You should then be able to visit a PHP file in your browser to test that it works. A simple test file would be:

    <?php
    $img = new Imagick('pic.jpg');
    $img->rotateImage('#fff', 45);
    header('Content-type: image/jpeg');
    echo $img;
    ?>

This is just a simple Imagick rotate on an image.
