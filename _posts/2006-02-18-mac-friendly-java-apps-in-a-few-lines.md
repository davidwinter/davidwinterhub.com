---
layout: post
title: 'Mac friendly Java apps in a few lines'
tags:
  - howto
  - java

---

Mac menu-bar? Set this before you initiate your `JFrame` in your `main` class:

    System.setProperty("apple.laf.useScreenMenuBar", "true");

Other systems will just ignore this and the menu-bar will appear as normal for that system.

Command keyboard button as default for keyboard shortcuts?

    yourJMenuItemObject.setAccelerator(
        KeyStroke.getKeyStroke( java.awt.event.KeyEvent.VK_O,
            Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));

This will get the default key for the operating system the Java app is running on. So that means 'Control' for Windows and 'Command' for Mac.

With those small things, you can make a Java app feel like a true part of the Mac family without sacrificing it working on other systems or the need for a separate binary.
