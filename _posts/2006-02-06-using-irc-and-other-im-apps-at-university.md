---
layout: post
title: 'Using IRC and other IM apps at University'
tags:
  - howto
  - irc
  - uni

---

At University, they've blocked ports for all IM applications, including IRC. This sucks, because IRC can actually be really helpful for asking for advice on things such as C++ or C. Also, our computing society has an IRC channel (#wmin-compsoc on Quakenet) and it was a real pain that we couldn't communicate with members at the University.

I'd seen Douglas Bowman's "how-to" on [secure email](http://www.stopdesign.com/log/2005/02/07/secure-email.html) using SSH tunnelling and thought that perhaps I could use this to get around the problem I was having. And, I'd be right in thinking that.

#### SSH rocks

I can't stress how cool SSH is - it let's me control my iMac at home while I'm at Uni, I can transfer my files backwards and forwards and *now* lets me connect to IRC.

On my iBook that I use at Uni, I've got [Colloquy](http://colloquy.info) as my preferred IRC client. In the Terminal on my iBook I enter in:

    ssh -N -L 6668:irc.quakenet.org:6667 davidwinter@xxx.xxx.xxx.xxx

Where `xxx.xxx.xxx.xxx` is the IP address of my home computer.

Once you've entered this command, entered your password for the SSH connection - leave the Terminal window open. When you're finished, hit Control C to quit SSH.

In Colloquy, I create a new connection with the following details:
* Chat Server: localhost
* Chat Server Port :6668

Connect using those details, and then you should be able to join `#wmin-compsoc` perfectly.

Problem solved. I'll try briefly to explain what the `ssh` command does. 
1. First of all, we want a `ssh` connection, but without a shell prompt of the computer we're connecting to. That's what the `-N` flag is for. 
2. `-L` flags this as a local port tunnel. 
3. You then first specify the port that you'll use on the computer your using (in my case, my iBook) to tunnel out of the network you're currently connected to.
4. Then the server/IP address you want to access.
5. Then the IP address that you'd normally connect to if you weren't having a firewall issue. For IRC, this is 6667.
6. Then you follow all of the above with the SSH connection, which in my case is to my iMac at home. The first part being my username on the computer, and then the IP address.

Basically, my iMac at home is being used as a bridge between Quakenet and the network at University. The whole connection is secure as it's being encrypted over ssh. So, there's no vulnerability for the University's network technicians to worry about.

Obviously for this all to work, you need some kind of 'bridge' computer online that you can connect to. Otherwise this isn't going to work.

Also, this could be used for getting around other kinds of firewall blocked applications. However, remember the connection will be significantly slower as first of all, it's all being encrypted, and secondly because it's having to go via a bridge computer.

#### Meebo!

As I said, I could use ssh port tunnelling to connect to MSN, iChat etc. But why bother when I can use the very cool [Meebo](http://meebo.com). It's a Web 2.0 application that let's you connect to MSN, AIM, Yahoo! and Jabber instant messaging servers all at the same time, and simply through a web site. No need to download any software. Excellent.
