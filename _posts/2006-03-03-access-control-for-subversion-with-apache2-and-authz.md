---
layout: post
title: 'Access Control for Subversion with Apache2 and Authz'
tags:
  - apache
  - howto
  - subversion
  - ubuntu
  - uni

---

My group project at University now consists of three smaller projects that provide an overall RSS service. I want to let the guys work on these, while still letting me keep my other coursework jut accessible to me. At the moment, I just have basic http authentication set-up which isn't so great for pulling off what I want. 

Please welcome on stage the Apache2 mod, `authz_svn`...

If you followed my other [howto](http://davidwinter.me.uk/articles/2006/02/16/subversion-over-apache-2-on-ubuntu), you'll have all the pre-requisites for this.

First of all, we need to create an Access Control file.

    sudo nano /etc/apache2/svn_access_control

In this file, you'll want to put some rules. I'll first of all go over these and then provide some examples.

### Permissions

There are only two types of permission:

* Read only - `r` - a user can check-out a copy of a project.
* Read and Write - `rw` - a user can check-out and commit changes to a project.

### Users

These are the same usernames that you have set in your password file that you created in the previous [howto](http://davidwinter.me.uk/articles/2006/02/16/subversion-over-apache-2-on-ubuntu). You can always add more users to this file using:

    sudo htpasswd2 -m /etc/apache2/dav_svn.passwd bill

When prompted, enter the password for the user.

### Repository Location

You specify the above rules in certain locations for the repository. These go between square brackets.

    [/]

The above will specify rules for the root of the repository.

    [/wowapp/trunk]

The above will specify rules for a project named 'wowapp' in the trunk location.

### User Groups

You can create groups of users and then use those for rules. You do this under a special heading in square brackets:

    [groups]
    mygroup = dave, mike

This will create a group called 'mygroup' which 'dave' and 'mike' belongs to.

And now for some examples.

### Examples

    [groups]
    team = bob, bill
    devteam = bob, barry, brett
    
    [/]
    @team = r
    bob = rw
    
    [/wowapp/trunk]
    @team = r
    @devteam = rw
    brenda = rw

In this example:

* Created a group `team` which has two members; `bob` and `bill`.
* Created another group, called `devteam` which has three members; `bob`, `barry`, `brett`.
* In the root of the repository, I've given the group `team` read permissions.
* Also, in the root, `bob` has read and write permissions.
* In the trunk of `wowapp`, the group `team` has read permission. 
* Also, the `devteam` group has read and write permissions.
* And another user, called `brenda` has read and write permissions.

Once you've created your desired access controll file, save the changes in nano by hitting `CTRL O`, hit enter to save the name, then `CTRL X` to quit Nano.

We just need to now link this access control file with our Subversion set-up.

    sudo nano /etc/apache2/mods-enabled/dav_svn.conf

Here's the example from the previous how-to:

    <Location /svn>
      DAV svn
      SVNPath /home/svn

      AuthType Basic
      AuthName "Subversion Repository"
      AuthUserFile /etc/apache2/dav_svn.passwd
      Require valid-user
    </Location>

All you need to add is the following line:

    AuthzSVNAccessFile /etc/apache2/svn_access_control

So that the file looks like this:

    <Location /svn>
      DAV svn
      SVNPath /home/svn

      AuthType Basic
      AuthName "Subversion Repository"
      AuthUserFile /etc/apache2/dav_svn.passwd
      
      AuthzSVNAccessFile /etc/apache2/svn_access_control
      
      Require valid-user
    </Location>

Save the file, and then restart Apache2:

    sudo /etc/init.d/apache2 restart

You should now have access control working for Subversion over Apache2.
