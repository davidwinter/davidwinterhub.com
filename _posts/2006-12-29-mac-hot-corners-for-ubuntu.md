---
layout: post
title: 'Mac "Hot Corners" for Ubuntu'
tags:
  - apps
  - mac
  - ubuntu

---

I've had a bit more time recently to play around with Ubuntu and am finding the 'Ubuntu versions' of apps that I've grown to love on Mac OS X. One of which being Hot Corners which I always use to lock my screen when moving away from it.

[Brightside](http://packages.ubuntulinux.org/dapper/gnome/brightside) is the Ubuntu app to do the job.

    sudo apt-get install brightside

Once installed, go to System, Preferences, Screen Actions. I then checked the "Bottom right corner" and then chose "Start screensaver". Works a charm.

Be sure that in your Screensaver settings to check the "Lock screen when screensaver is active" option.
