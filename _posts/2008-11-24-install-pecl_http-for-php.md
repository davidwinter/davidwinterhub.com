---
layout: post
title: 'Install pecl_http for PHP'
tags:
  - howto
  - http
  - lighttpd
  - mac
  - macports
  - osx
  - pecl_http
  - php

---

You will probably want to ensure that `curl` supports `https` before getting underway:

    curl -V

Check that the output contains `https`:

    >: curl -V
    curl 7.19.2 (i386-apple-darwin9.5.0) libcurl/7.19.2 OpenSSL/0.9.8i zlib/1.2.3
    Protocols: tftp ftp telnet dict http file https ftps 
    Features: Largefile NTLM SSL libz

If it doesn't:

    sudo port deactivate curl
    sudo port install curl +ssl

Once you have `curl` with `https` support:

    sudo pecl install pecl_http

If you followed my previous [howto](/articles/2008/11/22/php5-lighttpd-and-imagick-on-mac-os-x-leopard/), you'll want to move the module to the location specified in your `php.ini` file:

    sudo cp /opt/local/lib/php/extensions/no-debug-non-zts-20060613/http.so /opt/local/lib/php/extensions

Restart `lighttpd` and then you'll be good to go.
