---
layout: post
title: 'lighttpd and PHP on CentOS 5'
tags:
  - centos
  - howto
  - lighttpd
  - php

---

Here's a quick run down on getting [lighttpd](http://www.lighttpd.net/) and [php](http://php.net) running on [CentOS 5](http://www.centos.org/).

    yum install lighttpd lighttpd-fastcgi php
    mkdir /var/run/lighttpd
    touch /var/run/lighttpd/php-fastcgi.socket
    chown -R lighttpd:lighttpd /var/run/lighttpd/
    vim /etc/php.ini

Add in `php.ini` the following; `cgi.fix_pathinfo = 1`. Save and close the file.

Now open up `/etc/lighttpd/lighttpd.conf`. Uncomment the following line `"mod_fastcgi",` from the `server.modules` option. Then ensure that the following is uncommented:

    fastcgi.server  = ( ".php" =>
        ( "localhost" =>
            (
                "socket" => "/var/run/lighttpd/php-fastcgi.socket",
                "bin-path" => "/usr/bin/php-cgi"
            )
        )
    )

Save the file. Now create a test file, `/srv/www/lighttpd/index.php` and just include `<?php phpinfo(); ?>`. Now open your [favourite browser](http://getfirefox.com) and visit [http://localhost/index.php](http://localhost/index.php). You should be greeted with a friendly PHP page, and you're set to go.
