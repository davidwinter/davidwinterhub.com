---
layout: post
title: 'PHP and nginx on Ubuntu: the easy way'
tags:
  - howto
  - nginx
  - php
  - ubuntu

---

I've now changed my slice from running lighttpd to nginx. Here's the simplest way, in around 6 commands, to get PHP up and running via FastCGI.

## Install and setup

Install PHP 5:

    sudo aptitude install php5-cgi

Install nginx:

    sudo aptitude install nginx

Create PHP 5 FastCGI start-up script:

    sudo nano /etc/init.d/php-fastcgi

Inside, put:

    #!/bin/bash
    BIND=127.0.0.1:9000
    USER=www-data
    PHP_FCGI_CHILDREN=15
    PHP_FCGI_MAX_REQUESTS=1000

    PHP_CGI=/usr/bin/php-cgi
    PHP_CGI_NAME=`basename $PHP_CGI`
    PHP_CGI_ARGS="- USER=$USER PATH=/usr/bin PHP_FCGI_CHILDREN=$PHP_FCGI_CHILDREN PHP_FCGI_MAX_REQUESTS=$PHP_FCGI_MAX_REQUESTS $PHP_CGI -b $BIND"
    RETVAL=0

    start() {
          echo -n "Starting PHP FastCGI: "
          start-stop-daemon --quiet --start --background --chuid "$USER" --exec /usr/bin/env -- $PHP_CGI_ARGS
          RETVAL=$?
          echo "$PHP_CGI_NAME."
    }
    stop() {
          echo -n "Stopping PHP FastCGI: "
          killall -q -w -u $USER $PHP_CGI
          RETVAL=$?
          echo "$PHP_CGI_NAME."
    }

    case "$1" in
        start)
          start
      ;;
        stop)
          stop
      ;;
        restart)
          stop
          start
      ;;
        *)
          echo "Usage: php-fastcgi {start|stop|restart}"
          exit 1
      ;;
    esac
    exit $RETVAL

Make start-up script executable:

    sudo chmod +x /etc/init.d/php-fastcgi

Launch PHP:

    sudo /etc/init.d/php-fastcgi start

Launch at start-up:

    sudo update-rc.d php-fastcgi defaults

That's it. All installed and ready to go.

## Test

In your server config, add the following:

    location ~ \.php$ {
        fastcgi_pass    127.0.0.1:9000;
        fastcgi_index   index.php;
        fastcgi_param   SCRIPT_FILENAME /var/www/nginx-default$fastcgi_script_name;
        include         fastcgi_params;
    }

Restart nginx:

    sudo /etc/init.d/nginx restart

Create a file in your web root (in the example above, /var/www/nginx-default/test.php):

    <?php
    
    phpinfo();

Visit the page in your browser and you should see the standard PHP info page. And you're done.

Source: [Aberration](http://tomasz.sterna.tv/2009/04/php-fastcgi-with-nginx-on-ubuntu/)
