---
layout: post
title: 'Mod rewrite rules in htaccess files'
tags:
  - apache
  - httpd

---

Rewrite rules in an htaccess file don't need to leading slash, otherwise they won't work.

Be warned. Don't waste two hours wondering why it isn't working.
