---
layout: post
title: 'Apache, PHP and MongoDB on Mac OS X 10.6 Snow Leopard'
tags:
  - apache
  - mac
  - macports
  - mongodb
  - osx
  - php
  - web

---

[MongoDB](http://www.mongodb.org/display/DOCS/Home) (from "humongous") is a scalable, high-performance, open source, schema-free, document-oriented database.

There's a lot of [buzz](http://delicious.com/search?p=mongodb&chk=&context=popular|&fr=del_icio_us&lc=) brewing about it, so I wanted to give it a try with PHP on my development Mac. The following is how I went about installing Apache, PHP and MongoDB on Snow Leopard. You must have installed the [Xcode]([http://developer.apple.com/tools/xcode/) developer tools (found on the Snow Leopard install DVD) and [MacPorts]([http://www.macports.org/) for this to all work.

First, let's install Apache:

	sudo port install apache2

Nice and simple. Now we can move onto PHP:
	
	sudo port install php5 +apache2 +pear
	
To enable PHP for Apache:

	cd /opt/local/apache2/modules
	sudo /opt/local/apache2/bin/apxs -a -e -n "php5" libphp5.so

I like to make some alias commands for starting, restarting and stopping Apache:

	mate ~/.bashrc
	
	alias apache-start="sudo /opt/local/apache2/bin/apachectl start"
    alias apache-restart="sudo /opt/local/apache2/bin/apachectl restart"
    alias apache-stop="sudo /opt/local/apache2/bin/apachectl stop"

	source ~/.bashrc

Now we can make a few changes to the Apache config so that we can use our Sites directory:
	
	sudo mate /opt/local/apache2/conf/httpd.conf
	
I'm using Textmate here (`mate`), but you can use any editor you wish, like `nano`.

Change the `DocumentRoot`:
	
	DocumentRoot "/Users/davidwinter/Sites"
	
Then update the `<Directory>` path to the updated `DocumentRoot` path.	
	
	<Directory "/Users/davidwinter/Sites">
		
We also want to enable `index.php` as a directory index file:

	<IfModule dir_module>
	    DirectoryIndex index.html index.php
	</IfModule>

And then add the PHP mime type:

	<IfModule mime_module>
		...
	    AddType application/x-httpd-php .php
	    AddType application/x-httpd-php-source .phps
	</IfModule>

Now we need to setup the `php.ini` file:
	
	sudo cp /opt/local/etc/php5/php.ini-development /opt/local/etc/php5/php.ini
	
	sudo mate /opt/local/etc/php5/php.ini

My [timezone]([http://php.net/manual/en/timezones.php) is for London:

	date.timezone = Europe/London

Now we can get onto installing MongoDB (this will install the latest stable version as of time of writing - 1.2.1):	
	
	sudo port install mongodb

And then the PHP MongoDB extension:

	sudo pecl install mongo
	
You'll need to add the extension to the bottom of your `php.ini` file:
	
	sudo mate /opt/local/etc/php5/php.ini
	
	extension=mongo.so

Now startup Apache using the `alias` created earlier:

	apache-start

If you create a file in your Sites directory, called `test.php`, and add the following to it:

	<?php phpinfo(); ?>

Then visit the page in your browser; [http://localhost/test.php](http://localhost/test.php)

You can then scroll down the page and you should see a section titled 'mongo'. This means the extension is working!

Now we need to startup MongoDB:

	mkdir -p ~/data/db
	mongod --dbpath ~/data/db/

Almost there, now a simple MongoDB test in PHP:

	<?php
	// Connect:
	$connection = new Mongo();
	// Select database:
	$db = $connection->my_db;
	// Select collection:
	$films = $db->films;
	
	$frwl = array(
		'title' => 'From Russia With Love',
		'year' => 1963,
		'actor' => 'Sean Connery'
	);
	// Save array to collection:
	$films->insert($frwl);
	
	$gf = array(
		'title' => 'Goldfinger',
		'year' => 1964,
		'actor' => 'Sean Connery',
		'girl' => 'Pussy Galore'
	);
	
	$films->insert($gf);
	
	// Count documents in collection:
	if ($films->count()): ?>
	<table>
		<tr>
			<th>Title</th>
			<th>Year</th>
			<th>Actor</th>
			<th>Girl</th>
		</tr>
		<?php foreach ($films->find() as $film): ?>
		<tr>
			<td><?php echo $film['title']; ?></td>
			<td><?php echo $film['year']; ?></td>
			<td><?php echo $film['actor']; ?></td>
			<td><?php echo $film['girl']; ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
	<?php endif;
	?>

Very exciting stuff. More to follow.
