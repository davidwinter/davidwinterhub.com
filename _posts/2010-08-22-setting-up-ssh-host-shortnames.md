---
layout: post
title: 'Setting up SSH host shortnames'
tags:
  - cli
  - linux
  - mac
  - osx
  - ssh
  - unix

---

Here's an example setup to create SSH host shortnames. On you local computer, add the following to `~/.ssh/config`:

	Host server1
	HostName server1.internet.com
	User david

	Host server2
	HostName server2.internet.com
	User david

	Host *
	User davidwinter

Now with this file saved, you can ssh into `server2.internet.com` with just the following command:

    ssh server2

This will save you having to type out:

    ssh david@server2.internet.com

And if you have [public key authentication](/articles/2010/08/22/ssh-and-public-key-authentication/) setup, it makes the process even smoother. Adding the wildcard host record at the bottom allows you to specify a default username to use for other servers to the ones you've not specified above.
