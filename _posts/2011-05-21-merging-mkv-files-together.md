---
layout: post
title: 'Merging MKV files together'
tags:
  - cli
  - mac
  - mkv
  - unix

---

    mkvmerge -o merged_file.mkv file1.mkv + file2.mkv
