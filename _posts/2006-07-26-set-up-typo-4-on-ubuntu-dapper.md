---
layout: post
title: 'Set-up Typo 4 on Ubuntu Dapper'
tags:
  - howto
  - rails
  - typo
  - ubuntu

---

It was very annoying to find out that the brand spanking new installer for Typo wasn't going to install over rubygems.  The irritating part was that it was SQLite causing the problem because it's a dependency of the Typo gem. No idea why. But I don't want to SQLite - I'm running MySQL on Apache.

So anyway, here's how I got it working. Thanks to `sprewell` on the Typo IRC channel.

1. Download Typo from Subversion.

        svn co svn://typosphere.org/svn/typo/trunk typo

2. Make sure that you're inside that directory that was downloaded from Subversion.
    
        bin/typo config ~/typo web-server=external database=mysql

3. Grab the MySQL schema from `db/mysql.schema.sql` and manually run it on your MySQL database of choice.

4. Go into `config/` and make sure you get the `database.yml` file set-up to use your database settings.

5. Access your Typo install in your browser and you should get a prompt asking you for your admin details.

6. Blog away!
