---
layout: post
title: 'Switch to Static IP on Ubuntu Server'
tags:
  - command-line
  - home
  - howto
  - linux
  - ubuntu

---

I just bought a new Linksys router for my home network and wanted to set-up my Ubuntu Web Server with a static IP address so that I could port-forward a few things. Trouble is, with only the command line it's a little tricky to figure out exactly how to do it. Following are the steps I used.

    sudo nano /etc/network/interfaces

Inside the file, you'll see the following line:

    iface eth1 inet dhcp

We want to switch from `dhcp` to a `static` IP address. Comment or delete that line, and then add the following to the file:

    iface eth1 inet static
    address 192.168.1.200
    netmask 255.255.255.0
    gateway 192.168.1.1

1. This line states we want to use a static IP address.
2. The static IP address you want to use.
3. The subnet mask.
4. This is the IP address of my router which connects to the Internet.

Save the file and then restart the network settings:

    sudo /etc/init.d/networking restart

Job done.
