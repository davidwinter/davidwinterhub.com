---
layout: post
title: 'Install Mercurial on CentOS 4'
tags:
  - centos
  - mercurial
  - python

---

We've finally moved to Mercurial at work (well, we didn't exactly move from anywhere, but that's another story...). Our production server is running CentOS 4, which comes installed with Python 2.3.4. Mercurial requires 2.4. No Python updates available in the yum repository. What to do?

First up, install the latest version of Python to a location that won't interfere with the system:

	wget http://www.python.org/ftp/python/2.6.5/Python-2.6.5.tgz
	tar xvfz Python-2.6.5.tgz
	cd Python-2.6.5/
	./configure --prefix=/opt
	make
	sudo make install

Now we just want to test the new Python install:

    /opt/bin/python -V

This should come back with `Python 2.6.5`.

Now we install Mercurial, but tell it to use this new install of Python, rather than the default:

	cd ..
	wget http://mercurial.selenic.com/release/mercurial-1.5.1.tar.gz
	tar xvfz mercurial-1.5.1.tar.gz
	cd mercurial-1.5.1/
	sudo make install PYTHON=/opt/bin/python PREFIX=/opt

Now to test Mercurial has installed successfully:

    /opt/bin/hg --version

This should print out somewhere the version 1.5.1. Now Mercurial is setup, add it to `/usr/local/bin` so that it's available in the path:

    sudo ln -s /opt/bin/hg /usr/local/bin/hg

That wasn't too painful was it?
