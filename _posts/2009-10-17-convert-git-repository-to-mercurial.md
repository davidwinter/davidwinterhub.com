---
layout: post
title: 'Convert git repository to mercurial'
tags:
  - git
  - hg
  - mercurial

---

Ensure that the mercurial convert extension is enabled:

    nano ~/.hgrc

Inside that file add:

    [extensions]
    hgext.convert=

Save. Now do:

    hg convert my-git-repo

This will create a new directory called `my-git-repo-hg`. This will appear empty at first, so do this:

    cd my-git-repo-hg
    hg checkout

All of your files will appear and you're ready to go.
