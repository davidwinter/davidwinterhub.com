---
layout: post
title: 'DVB and Mplayer'
tags:
  - freeview
  - howto
  - mplayer
  - tv
  - ubuntu

---

As a follow up to [yesterday's post](/articles/2008/02/08/watching-freeview-dvb-t-tv-with-vlc-player-on-ubuntu/), here is a quick run through of how to use [Mplayer](http://www.mplayerhq.hu/design7/news.html) to view DVB channels.

Basically, you just need to generate a `channels.conf` file ([see this article](/articles/2008/02/08/watching-freeview-dvb-t-tv-with-vlc-player-on-ubuntu/)) and place it inside `~/.mplayer/`. Then run `mplayer dvb://` in a terminal window. You'll get a video window appear, and the first channel within your `channels.conf` file will start playing. To switch up and down between channels, you can press **h** and **k**. There's a [bunch of other helpful keyboard commands](http://www.keyxl.com/aaa2fa5/302/MPlayer-keyboard-shortcuts.htm) though not all of them will work with DVB.

While using Mplayer this way isn't very user friendly, it may be helpful for some.
