---
layout: post
title: 'A simple chat room with WebSockets'
tags:
  - howto
  - javascript
  - websockets

---

Sorry, I promised a blog post, but instead you get a link to bitbucket with the source of a basic chat app written using WebSockets.

[https://bitbucket.org/davidwinter/websockets-chat/src](https://bitbucket.org/davidwinter/websockets-chat/src)
