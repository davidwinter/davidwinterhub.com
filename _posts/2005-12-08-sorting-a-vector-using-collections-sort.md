---
layout: post
title: 'Sorting a Vector using Collections.sort()'
tags:
  - howto
  - java

---

`Collections.sort()` allows you to sort a [Collections](http://java.sun.com/j2se/1.5.0/docs/guide/collections/index.html) framework that is inherited from the [List](http://java.sun.com/j2se/1.5.0/docs/api/java/util/List.html) class. The [Vector](http://java.sun.com/j2se/1.5.0/docs/api/java/util/Vector.html) class is, so that's great!

For demonstrations sake, I'll be using a Vector that stores some products. I want to sort the products by price. I'll assume you'd include setter and getter methods for each data member rather than declaring them public. But again for ease of this tutorial, I've just made them `public`.

    public class Product
    {
        public double price;
        public String name;

        public Product()
        {
            this.price(0.0);
            this.name("");
        }
    }

We'd create a Vector of Products by using

    Vector<Product> products = new Vector<Product>();

So that we can sort the vector, we need a way of telling `Collections.sort()` how to compare our Products. For example, how it can tell that ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£1.99 is greater than ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£0.99. I've already chosen that we want to sort by price.

In order for us to do this, we need our Product class to implement the interface [`Comparable`](http://java.sun.com/j2se/1.5.0/docs/api/java/lang/Comparable.html). This interface contains only one method - [`compareTo()`](http://java.sun.com/j2se/1.5.0/docs/api/java/lang/Comparable.html#compareTo\(T\)) that must be implemented in our Product class. It's this function that `Collections.sort()` uses.

So, how exactly do we do that?

    public class Product implements Comparable
	{
		// other functions in this class
		
		public int compareTo(Object o)
		{
			
		}
	}

`compareTo()` will return an integer value that represents the comparison between two Product objects. `-1` will represent a Product that is less than the one we're checking against. `0` will represent an Product of equal values (in our case, that have the same price). And finally `1` will represent an Product that is more than the one we're checking.

In order to perform the check we need to compare the prices. These are represented in our class as `double`. The class Double already implements the `Comparable` interface, so that means we know that `compareTo()` is a method of the Double class. So we're going to use that to compare our prices.

	public int compareTo(Object o)
    {
    	Product e = (Product) o;

    	int result = this.price.compareTo(e.price);
    }

What this is doing is first of all getting the price of the Product object. This returns a double. We then apply the `compareTo()` method to that returned the double with the object given in the parameter. We call `e.price` which returns the double of the price for the compare object.

After doing this, we get an integer returned and stored into `result`. We need to check what this value is.

    public int compareTo(Object o)
    {
    	Product e = (Product) o;

    	int result = this.price.compareTo(e.price);

    	if (result < 0)
    	{
    		return -1;
    	}
    	else if (result == 0)
    	{
    		return 0;
    	}
    	else (result > 0)
    	{
    		return 1;
    	}
    }

If the result is less than 0, that means that the price of the current Product object is less than the one we're checking against. 0 will mean that the prices are equal. And more than 1 means the price is more than the current Product.

So based on the result value, we return a new integer value that represents the comparison between two Products. Less than 0 if the object is less than the one being compared. 0 if they are equal, and more than if it is more than the compared object.

At this stage we've implemented the `Comparable` interface for our Product class. This now means our Product objects can be compared against one another based on their prices.

Now we actually want to sort our Vector. We'll need to create the sort method.

In this example, I'm extending the Vector class:

    public class ProductList extends Vector<Product>
    {
    	public ProductList()
    	{
    	    super();
    	}

    	public ProductList sort()
    	{
    	    // sort method
    	}
    }

This is where `Collections.sort()` comes in. Now that our Product objects can be compared, the `Collections.sort()` static method can access our vector and compare the objects in it and re-sort it accordingly.

    	public ProductList sort()
    	{
    	    ProductList sorted = (ProductList) this.clone();
    	    Collections.sort(sorted);

    	    return sorted;
    	}

This will then return an updated ProductList object (which is the Vector storing our Products).

Bingo. 
