---
layout: post
title: 'Redirecting Apache traffic to a maintenance page'
tags:
  - apache
  - howto
  - web

---

Here's a simple solution to redirect users to a maintenance page in Apache. This rewrite rule can stay in your config (`<VirtualHost>` or `.htaccess`) all the time; all that you need to enable it is to create the file `maintenance.html`.

The first rewrite condition checks to see if the file exists, and only if it does, will it redirect all traffic to it. The second rewrite condition is there to prevent an infinite loop, by  only redirecting traffic to files other than `maintenance.html`.

	RewriteEngine On
	
	RewriteCond %{DOCUMENT_ROOT}/maintenance.html -f
	RewriteCond %{REQUEST_FILENAME} !/maintenance.html
	RewriteRule ^.*$	/maintenance.html [L] 
