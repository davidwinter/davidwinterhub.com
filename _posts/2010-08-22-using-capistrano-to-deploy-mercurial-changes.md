---
layout: post
title: 'Using Capistrano to deploy Mercurial changes'
tags:
  - capistrano
  - howto
  - linux
  - mac
  - osx
  - ruby
  - servers
  - ssh

---

We have a few production servers at work, and we have a central [bitbucket](http://bitbucket.org) repository to store our core code. Once we make a change on our testing server (!), we used to have to commit, push changes to bitbucket, and then ssh into each server, then pull changes, and update each repository. A pain in the backside! Enter [Capistrano](http://www.capify.org). A ruby ssh automation tool. In a few simple steps you can create a recipe file that will let you do this all with one command.

On the machine where you'd like to kick this all off---where you'll be pushing the changes from---install Capistrano (requires [Ruby](http://www.ruby-lang.org/en/) and [RubyGems](http://rubygems.org/) to be installed):

    sudo gem install capistrano

Capistrano is opinionated software; meaning it makes a [few assumptions](http://www.capify.org/index.php/Getting_Started) about your server setup which you'll need to comply with. Mainly that either you have [public key authentication](/articles/2010/08/22/ssh-and-public-key-authentication/) setup to connect to these remote servers, or, you use the same password across all of them.

Now, create a `capfile`, perhaps in your home directory, and here is an example that you can modify for your own setup:

	role :production_servers, "srv1.internet.com", "srv2.internet.com"

	task :push_deploy, :roles => :production_servers do
	    system "cd /local/path/to/repos && hg push"
	    run "cd /remote/path/to/repos && hg pull && hg update"
	end

With that now saved, you can issue this task with the following command:

    cap push_deploy

This will first of all run a local command to push the changes to bitbucket. After that is complete, it'll connect to each of the production servers listed, pull changes and then update the repository. And that's it. You don't need to do anything else. Piece of [cake](http://fatcakehole.tumblr.com/).
