---
layout: post
title: 'Getting Ubuntu Dapper to dance with ATI X800 GTO'
tags:
  - dapper
  - howto
  - ubuntu

---

I've made a brand new spare PC that I'm hoping my parents can use in their spare room/study. I refuse to install Windows, but as everyone knows, I'm a keen Ubuntu fan.

My younger brother donated me his "old" ATI Radeon X800 GTO graphics card to use in it (I want to eventually get XGL + Compiz set-up on it for all the neat eye candy). Thing is, Ubuntu and the card don't play nice straight away. Here's how I got them to dance.

**Note:** This was from a fresh install of Ubuntu Dapper.

Boot from the Ubuntu install CD and when you're presented with the install options, select "Safe Graphics Mode". This will let Ubuntu run using your ATI card and some safe graphics drivers - you only need this so that you can complete the installation.

When the live CD environment has loaded and you see the desktop, run the 'Install' program from the desktop as-per-normal. Once it's completed it's best to upgrade Dapper to the latest version. You can either do this from the Terminal or via the notification bubble that pops up in the menu bar:

    sudo apt-get update

You'll probably need to reboot once that's done because a new kernel (I'm guessing) has been installed. Once you've rebooted, type in a Terminal window:

    sudo apt-get install xorg-driver-fglrx
    sudo aticonfig --initial
    sudo aticonfig --overlay-type=Xv

This downloads and sets up the ATI proprietary drivers for you.

Now just restart the window environment by hitting CTRL + ALT + BACKSPACE on your keyboard. The screen will go to a normal command line login type screen for about 30 seconds or less. Then the desktop will appear again. You're now running Ubuntu with the ATI drivers. Bravo!

This was a must for me as I have bought a a nice 19" widescreen flatscreen monitor - and unless I installed the drivers, I couldn't get the nice big resolutions. Now I have a lovely 1440 x 900 res. Lovely jubbly.
