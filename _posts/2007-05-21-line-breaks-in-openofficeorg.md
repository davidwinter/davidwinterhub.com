---
layout: post
title: 'Line breaks in OpenOffice.org'
tags:
  - openofficeorg
  - uni

---

A really frustrating issue I had when writing my project report, was that of dealing with source code formatting. 

I had created a new style for source code similar to that I use on my blog; a box with a border and coloured background. The problem I was having was when pasting source code into the document. Each line had a _paragraph_ break between itself and the next, which meant that there was a spacing between each. To fix this, I had to insert a manual _line_ break between each line. Luckily I found a shortcut to pull this off, which was `Shift` + `Enter`.

Does anyone know of an easier way of pasting in text and using line breaks rather than paragraph breaks?
