---
layout: post
title: 'Me TV - EyeTV for Ubuntu?'
tags:
  - eyetv
  - freeview
  - mac
  - me-tv
  - switch
  - tv
  - ubuntu

---

My search for an open source, Ubuntu alternative for EyeTV on OS X is nearly over. I stumbled across '[Me TV](http://me-tv.sourceforge.net/index.html)' a few days ago--and I've left it as long before writing up a post about it so that I could have a good play with it.

## Installation

For those using Gutsy, you'll need to add the Launchpad repository to your Apt sources:

    sudo nano /etc/apt/sources.list

Add the following to the bottom of the file:

    # Me TV
    deb http://ppa.launchpad.net/michael-lamothe/ubuntu gutsy main
    deb-src http://ppa.launchpad.net/michael-lamothe/ubuntu gutsy main

Save the file, and then:

    sudo apt-get update
    sudo apt-get install me-tv
    
<del datetime="20080211">If you're running Hardy, Me TV is in the Universal repository, so you can just run `sudo apt-get install me-tv`.</del> <ins datetime="20080211">**Update:** After contacting Michael, the developer of Me TV, he's said that the package in the Hardy Universe repository is out of date. For the time being, you're better off adding the LaunchPad repository, as above.</ins>

## Features

Just a reminder of the features that I'm looking for in an application to replace EyeTV:

 - Watch all Freeview (DVB-T) channels.
 - Be able to pause, rewind, and forward up to live TV.
 - Schedule programs to record.
 - See the TV listings for the channels I have (EPG)
 - Fullscreen or windowed viewing.

Out of all of the above, there is only one feature--'Be able to pause, rewind, and forward to live TV'--that isn't currently available with Me TV, though I believe it should be possible to implement it in the future with developer support. 

Here's a brief run down of those features:

### Watch all Freeview (DVB-T) channels

When you first launch the app, it asks if you would like it to create a `channels.conf` file, and then scans for the channels it can find. 

<div><a href="/wp-content/uploads/2008/02/scan.png" rel="lightbox[metv]" title="Me TV asks to scan for channels." class="image_link"><img src="/wp-content/uploads/2008/02/scan.thumbnail.png" alt="Scan Prompt" /></a></div>

<div><a href="/wp-content/uploads/2008/02/region.png" rel="lightbox[metv]" title="Me TV asks for the region closest to you." class="image_link"><img src="/wp-content/uploads/2008/02/region.thumbnail.png" alt="Region Prompt" /></a></div>

<div><a href="/wp-content/uploads/2008/02/channels-found.png" rel="lightbox[metv]" title="Me TV displays the channels it finds." class="image_link"><img src="/wp-content/uploads/2008/02/channels-found.thumbnail.png" alt="Channels Found" /></a></div>

Once this small step is complete, the main window opens with one of the channels on. You'll also notice below the EPG. You can switch channels by scrolling up and down the EPG and clicking on the channel name.

### Schedule Programs to Record

This is as simple as finding the program in the EPG, clicking on Record and then accepting the recording schedule. You can also set, if you want, Me TV to repeat the schedule, say; daily, weekly, etc.

<div><a href="/wp-content/uploads/2008/02/record.png" rel="lightbox[metv]" title="Me TV's recording confirmation." class="image_link"><img src="/wp-content/uploads/2008/02/record.thumbnail.png" alt="Recording" /></a></div>

<div><a href="/wp-content/uploads/2008/02/record-details.png" rel="lightbox[metv]" title="Me TV's record details dialog." class="image_link"><img src="/wp-content/uploads/2008/02/record-details.thumbnail.png" alt="Recording Details" /></a></div>

### Electronic Program Guide - EPG

This can be toggled on or off by right clicking in the window. You can scan through the listings every 3 hours, and switch back to what is currently on.

<div><a href="/wp-content/uploads/2008/02/epg.png" rel="lightbox[metv]" title="Me TV's EPG view." class="image_link"><img src="/wp-content/uploads/2008/02/epg.thumbnail.png" alt="EPG" /></a></div>

### Full Screened or Windowed Viewing

Simply double click in the main window. Voila.

<div><a href="/wp-content/uploads/2008/02/tv.png" rel="lightbox[metv]" title="Me TV's standard TV view." class="image_link"><img src="/wp-content/uploads/2008/02/tv.thumbnail.png" alt="TV" /></a></div>

## Some Suggestions

Me TV is already very user friendly, however, there are a few small things that I think could be altered or added that would make the experience even better:

 - On the initial launch, rather than asking if Me TV should create a `channels.conf` file, instead why not just a notification such as; 'Me TV needs to scan for channels that you can watch.' Non-technical users may get intimidated by the question about a `channels.conf` file. Me TV _has_ to scan for channels. Just let the user know that's what it's doing.
 - For the region selection, include a drop down box for the various countries, and then display the regions for that selected country below--minus the country prefix. 
 - Ditch the 'Found Channel' text when scanning for channels. Just show the channel name that has been found.
 - Move the EPG into it's own window. It always seems as though the TV and EPG are competing against eachother. You always have to resize the window so that you can drag the EPG sizer up so that you can get a good glance at what is on across the channels. Then, when you close the EPG, the TV window is perhaps too big for viewing, so you then have to shrink the window again.
 - By moving the EPG into it's own window, it doesn't have to be restricted by the size of the TV window that you want. Users can make it larger, and therefore, rather than sticking to the 3 hour step between programs, why not just include a full 12/24 hours. At the top of the EPG window, have a button to skip by day. You can see a lot more of what's on in one glance with a larger window, than having to scroll around with a smaller one.
 - When recording a program from the EPG, after clicking the Record button, don't show the detailed schedule window afterwards. In most cases, I would bet that the user doesn't need to see or change the details. And if they do, they can go to the scheduler to do that. You only really need the details if you're manually creating a recording, or want to set a repeat.
 - For quick channel change, perhaps include a drop down box at either the top or bottom of the TV window. This removes the dependancy on switching to the EPG to switch channels, as you currently have to.
 
Some of these are very small, or rather picky suggestions, but none-the-less more user friendly.

## Conclusion

Overall, Me TV is an __excellent__ application. Obviosuly a lot of time has gone into developing it. If the feature set remained as it was, I would use it from now on, though I believe there are a few things that would make it an 'easier' experience for the user. And the one feature missing that would make it the best ever in Linux, and, hands down, the EyeTV alternative for OS X converts to Ubuntu, would be pause, rewind and forward to live TV.

I'll be suggesting Me TV to all that I know from now on!
