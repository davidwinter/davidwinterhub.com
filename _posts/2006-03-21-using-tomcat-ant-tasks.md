---
layout: post
title: 'Using Tomcat Ant tasks'
tags:
  - howto
  - java
  - tomcat

---

I'm now really loving Ant for compiling my Java projects together. It really has solved the problem of figuring out what to update, or going through countless commands to compile each package etc.

You could be wondering why I don't just use a fancy IDE, like Eclipse to do all this for me? Well... I've tried. But... I just can't 'like' Eclipse. I admit, I was very impressed with it - tons of functionallity, but in the end, I just couldn't give up using the beautiful TextMate. Eclipse felt cluttered and I was claustrophobic using it.

Pathetic excuse I know, but I need something I can get on and work with.

Anyway - enough side-tracking on talk of Eclipse.

Ant. Java compiling made easy. Bring on Tomcat extension tasks with it and that saves me another aspirin for working on my Tomcat coursework which is to develop a web based quiz. To install the Tomcat tasks for Ant on Mac OS X, you just need to copy them over:

    cp /usr/local/apache-tomcat-5.5.16/server/lib/catalina-ant.jar /Developer/Java/Ant/lib

Note that this installs the Tomcat tasks to the default Developer Tools install of Ant.  If you've installed Ant manually, you'll need to change the paths above accordingly.

Once the `.jar` is copied over, in your `build.xml` files for your various projects, you'll need to include the following tags:

    <taskdef name="deploy" classname="org.apache.catalina.ant.DeployTask" />
    <taskdef name="undeploy" classname="org.apache.catalina.ant.UndeployTask" />

These task definitions only cover `deploy` and `undeploy` tasks. To use them in your project:

    <target name="undeploy">	
    	<undeploy url="${tomcat.manager}"
    		username="${tomcat.username}"
    		password="${tomcat.password}"
    		path="/yourwebapp" />
    </target>

Where `tomcat.manager` is your URL to your Tomcat manager. Usually `http://localhost:8080/manager`. `tomcat.username` and `tomcat.password` are self-explanatory. Then just substitute `yourwebapp` with the name of your tomcat application directory located inside the `webapp` directory. For my current coursework, this is `webquiz`.

The `deploy` task is very similar:

    <target name="deploy" depends="war">
    	<deploy url="${tomcat.manager}"
    		username="${tomcat.username}"
    		password="${tomcat.password}"
    		path="/${ant.project.name}"
    		update="true"
    		localWar="${war.dir}/${ant.project.name}.war" />
    </target>
    
I have another target that creates a WAR file of my entire web application. This is what I'll use to deploy my application. The only additional attributes in the `deploy` task are `update` and `localWar`. `update` will replace an existing application if it already exists so that you have the latest working copy. `localWar` is the location of the WAR file you want to deploy.

Simple. Hope I've explained this clear enough.

As of writing this, I'm having some trouble getting this working with my iBook, which I assumed had the same set-up as my Intel iMac - of which, the above tasks are working perfectly.
