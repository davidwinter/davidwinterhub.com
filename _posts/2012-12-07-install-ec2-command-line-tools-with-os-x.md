---
layout: post
title: Install EC2 command line tools with OS X
description: Be a ninja with EC2 using the CLI
---

I've been using EC2 a lot recently, probably creating and destroying around 20 instances trying out a project, today alone. I needed to be able to do this a lot faster than the web console lets me, so I downloaded the EC2 command line tools to give me that bit extra.

## Installing

It's very easy to setup:

  1. Grab the zip file from the [AWS developer site](http://aws.amazon.com/developertools/351)
  2. Unzip to your home directory
  3. Rename the extracted directory to `.ec2`
  4. Go to the [Security Credentials](https://portal.aws.amazon.com/gp/aws/developer/account/index.html?action=access-key) page on the AWS site
  5. Select the **X.509 Certificates** tab
  6. Click on **Create a new Certificate**
  7. Download the two files in the popup to your `~/.ec2` directory

## Configuring to work with your CLI

Now we need to configure our shell--[Fish](http://ridiculousfish.com/shell/) in my case--to use the EC2 tools. Copy the following into your `~/.config/fish/config.fish` file:

	set PATH $EC2_HOME/bin $PATH
	set -g -x JAVA_HOME /System/Library/Frameworks/JavaVM.framework/Home/
	set -g -x EC2_HOME ~/.ec2
	set -g -x EC2_URL https://eu-west-1.ec2.amazonaws.com
	set -g -x EC2_PRIVATE_KEY (ls {$EC2_HOME}/pk-*.pem)
	set -g -x EC2_CERT (ls {$EC2_HOME}/cert-*.pem)

If you're using `bash` or `zsh`, it's a similar setup - just create the variables, and update your `PATH`. Here's `bash` (add this to your `~/.bash_profile` file):

	export PATH=$PATH:$EC2_HOME/bin
	export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Home/
	export EC2_HOME=~/.ec2
	export EC2_URL=https://eu-west-1.ec2.amazonaws.com
	export EC2_PRIVATE_KEY=`ls $EC2_HOME/pk-*.pem`
	export EC2_CERT=`ls $EC2_HOME/cert-*.pem`

**Note:** I've set my default region to `eu-west-1`. You may want to set this to something different. Change the `EC2_URL` variable accordingly.

Open a new tab or terminal window and the changes should now be present. To check, you can run:

	ec2-describe-instances

You should get a list of the instances currently associated to your account in your default region.

## Taking the tools for a spin

A good way to test the tools is to use my previous post on [getting started with EC2](/articles/2012/12/06/getting-started-with-amazon-ec2), but rather than using the web AWS console, we'll use nothing but the command line tools.

Create the key pair:
	
	ec2-create-keypair blog | sed 1d > blog.key

The output of this file includes a fingerprint of the key at the top, which we don't want, so we use `sed` to remove that line so we can easily save it to a file. We then just set restrictive permissions:

	chmod 600 blog.key

Create the security group:

	ec2-create-group blog -d Blog
	ec2-authorize blog -P tcp -p 22
	ec2-authorize blog -P tcp -p 80

Create and run the instance:

	ec2-run-instances -t t1.micro -g blog -k blog ami-e9eded9d

From the output of this command, get the instance ID, which is prefixed with `i-`.

Assign a dedicated IP:

	ec2-allocate-address

The above command will output an IP address. Copy that to your clipboard, and use it in the following command (paste it in place of `123.456.789.012`):

	ec2-associate-address -i i-INSTANCE_ID 123.456.789.012

You can then ssh into the instance with:

	ssh -i blog.key ubuntu@123.456.789.012

All done.