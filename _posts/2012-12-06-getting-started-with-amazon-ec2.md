---
layout: post
title: Getting started with Amazon EC2
description: It's not as difficult as it may first seem
---

A few friends have asked me to talk through how to setup an Amazon EC2 server. 

At first glance, it's very intimidating, confusing, and not quite clear where to start. But once you know about all the pieces that bring together the AWS services, it's not difficult at all to get a server up and running.

The great thing about EC2 is that the servers are virtualised. That means you can create and throw them away when not needed very easily. This makes it great for testing out things, not having to worry that you'll have to pay out for a full server for a month. Boot up an EC2 server, run it for a few minutes, or non-stop. It's up to you. You have complete freedom and flexibility.

Ready to tuck in?

## Some definitions

We'll start off with defining some of the core pieces that you'll need to be able to create, and then run an EC2 server:

* _EC2_ & _Instances_ 
  
  EC2 stands for Elastic Cloud Compute. This service allows you to setup virtual machines (known as instances) to run an operating system of your choice (mine is Ubuntu).

* _EBS_
  
  Elastic Block Store, these are the virtual hard drives your instance will use.

* _AMI_
  
  Amazon Machine Image, these are best described as base installs of an operating system, with a default configuration set and ready for the EC2 service.

* _Security Groups_
  
  This is where you can setup what is essentially a firewall for your instance. You say what ports are open on it. This means in the majority of cases you don't need to setup a software firewall on your install (such as `iptables` or `ufw`). It saves a lot of headaches.

* _Key Pairs_
  
  These are the SSH keys that you'll use to connect to your instance. When you create the machine, the public key of the selected pair is already added to the `authorized_keys` file, and you then download and use a private key to connect.

* _Elastic IPs_
  
  Allows you to assign a dedicated IP to the instance. Not required, but helpful for when ready to launch, and want to point a domain name.

## Getting started

Before you can do anything, you need to have an AWS account. Register if you don't already have one, then head to the [AWS Console](https://console.aws.amazon.com/console/home).

I've already mentioned that Ubuntu is my OS of choice, so we need an AMI to base our instance on. I always use the official Cannonical (who manage Ubuntu) ones, and they have a [helpful webpage](http://cloud-images.ubuntu.com/locator/ec2/) for you to find the correct AMI-ID. You need this ID in order to tell EC2 which AMI to use.

I want to use the latest version of Ubuntu (12.10 at time of writing), with a 64 bit processor, based in the EU region (which is in Ireland) with an EBS hard drive.

To narrow down the list of available AMI's, I type in '12.10 amd64 eu ebs' into the [search box](http://cloud-images.ubuntu.com/locator/ec2/). We're then left with one AMI - `ami-e9eded9d`.

## Creating the instance

You'll notice that the ID is a link which you can click on to start creating your EC2 instance. It directs you onto the AWS console.

The first screen gives you a summary of the AMI and the defaults for the hard drive setup - which is 8GB. This can be increased at a later time if needed. Click 'Continue'.

The important part on this step is selecting the size of the instance. It should be defaulted to 'T1 Micro'. Amazon [offer a variety of sizes](http://aws.amazon.com/ec2/instance-types/) that increase the power and RAM for your instance. The bigger the instance, the more you pay. Again, these can be changed at a later point. Ensure 'T1 Micro' is selected and then continue.

We won't be choosing any advanced options, so skip the next step. We'll also leave the default hard drive configuration, so skip the following step too.

The next screen allows you to name the instance. You'll see a key with the name prefilled 'Name', and you can set the value to something descriptive. If you're running a blog on the instance perhaps call it 'blog'. Click 'Continue'.

When the instance is running, you'll need to be able to connect to it via SSH so you can configure it. This is where SSH key pairs come into play. You'll want to create a new key pair. You just need to give it a name, and then click on the 'Create & Download your Key Pair'. Depending on what you call the Key Pair, for example, 'blog', a file called `blog.pem` will download. Keep this somewhere safe and secure. 

**Note:** It'd be a good idea to add a passphrase to this key file so that if someone did get the key, they couldn't use it to connect to your server without the passphrase. For this demonstration, we'll leave it as is, without. There's a good writeup on [Github](https://help.github.com/articles/working-with-ssh-key-passphrases) on why and how to use and create them.

Next we need to configure the Security Group for the instance. Create a new one and give it a name and description. Then you can either choose from the dropdown for common port configurations, or set something custom. We'll enable SSH (so we can connect to it remotely) and HTTP (because we'll probably be running a web server) for this instance. Select both of these, and add them to the Security Group. Click 'Continue'.

You're now on the final step before creating and starting the instance. You can check all of the configuration options we've set from here, and then you're ready to launch. Click 'Launch'.

AWS will tell you that your instance is now launching. Click on 'Close'.

In the left hand menu of the console, select 'Instances'. You should see a 'running' status for the instance you just created it. Select it from the list.

In the details section of the page near the bottom, you'll see your instance has been selected, and it will give you a hostname you can use to connect to your instance with. It'll look something like `ec2-12-345-67-890.eu-west-1.compute.amazonaws.com`.

Before we connect, you need to give the private key you downloaded earlier strict, private permissions for your user only.

	chmod 600 path/to/blog.pem

Now you can connect to your instance:

	ssh -i path/to/blog.pem ubuntu@ec2-12-345-67-890.eu-west-1.compute.amazonaws.com

You'll notice the user I'm connecting with is called `ubuntu`. This is the default user that the AMI we're using has setup. You should be promted to add the host to your known hosts file, so type in `yes` and hit enter.

And then, you're in! Connected to your brand new instance, with a fresh install of Ubuntu 12.10 on it.

## Give your instance a dedicated IP

You can now logout from SSH, and then we'll assign a dedicated IP to the instance, which you'd then be able to use to point a DNS A record to the instance.

In your AWS console, select Elastic IPs. Select 'Allocate New Address' and EC2 should be selected in the dropdown. Click 'Yes, Allocate'. With the new IP selected, click on 'Associate Address', choose the instance you've just created and then click on 'Yes, Associate'.

Copy that new IP address to your clipboard, and then you can connect to your instance again with that:

	ssh -i path/to/blog.pem ubuntu@123.456.789.012

In this example, we're using `123.456.789.012` as that newly created IP. You should get the known hosts prompt again, type `yes`, and then you'll be connected again.

That's it. You're ready to go.

## Aditional notes

You pay for what you use on AWS. And the pricing is a bit complicated because of the level of detail at which Amazon charge you for. Processing time, bandwidth used, disk space consumed, IP addresses, etc etc.

A handy calculator is available to work out how much you're likely to pay: [http://calculator.s3.amazonaws.com/calc5.html]().

While you're developing with AWS, it's probably a good idea to turn off your instance while you're not using it. You can do this from the console by right clicking on the instance and clicking on 'Stop'. To boot it up again, do the same but select 'Start' instead. If you want to delete the instance because you won't be using it again, select 'Terminate'. That'll delete the data on the instance, so terminate carefully.