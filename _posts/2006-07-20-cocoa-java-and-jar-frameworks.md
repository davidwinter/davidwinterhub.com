---
layout: post
title: 'Cocoa-Java and Jar Frameworks'
tags:
  - howto
  - java
  - mac
  - xcode

---

Note: Didn't have time to really write this article up - maybe some time I will - but I'm posting it here just in case it comes in handy for someone.

Now that I'm finished with coursework, and just awaiting my exams to start on Wednesday (can't wait!) - I've got a chance to work on a little application I've been meaning to write for the last year or so. I've just never had the opportunity to get on with it.

Most of this year, I've been learning to develop Java applications using Swing. However, the application I'm developing is for the Mac only. I want to be using the Cocoa-Java bridge for the time being as I can't find an RSS parsing framework as nice as Rome, which is written in Java.

So, I want to include the .jar file that holds the Rome framework in my Xcode application. This assumes you have a Cocoa-Java application set-up in Xcode.

1. Project > Add To Project
2. Select the file
3. New Build Phase
4. Select Executable
5. Drag jar to new build phase.
6. Project > Edit Active Target
7. Cocoa Java Specific
8. Change Root Directory to 'Contents'
9. Change app.jar to Resources/Java/app.jar
10. Add desired Jar file by adding MacOS/rome.jar
