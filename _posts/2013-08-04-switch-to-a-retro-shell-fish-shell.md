---
layout: post
title: Switch to a retro shell, Fish shell
description: Leave behind the millions of plugins, and slowness, and just use Fish
---

I've been using [fish shell](http://fishshell.com/) for the last year, and would never go back to using `bash` or `zsh` again. Ever.

All I want is for my shell to be speedy, and helpful. As standard. Without the need to install a mountain of plugins, or have a 200 line configuration file.

Fish is perfect for me. And I'm sure it'll be a perfect match for you too. Some highlights:

 * Invalid commands; if you mistype `mkdir` and type in `mkdi` or something similar, the command will appear in red. Instant feedback.
 * Valid filepaths appear underlined.
 * Autosuggest; while typing a command, autosuggest will appear in grey to the right of your cursor.
 * Autosuggest knows about paths and options, and will also pull in your history too.

![Fish shell demo](/images/fish-shell-demo.gif)

Your config is in a bit of an obscure location (the only negative I can think of) at `~/.config/fish/fish.config`.

If you're moving from `bash` or `zsh`, probably the most important thing you'll want to set, is your `$PATH` variable. Here's how in `fish`:

	set PATH "/usr/local/bin" $PATH

This is just prefixing `/usr/local/bin` to the front of my `$PATH` so my `homebrew` works.

Set your default `EDITOR`?

	set -g -x EDITOR vim

Want to display your current branch when in either a git or Mercurial repo?

	function git_prompt
	    if git root >/dev/null 2>&1
	        set_color normal
	        printf ' on '
	        set_color magenta
	        printf '%s' (git currentbranch ^/dev/null)
	        set_color normal
	    end
	end

	function hg_prompt
	    if hg root >/dev/null 2>&1
	        set_color normal
	        printf ' on '
	        set_color magenta
	        printf '%s' (hg branch ^/dev/null)
	        set_color normal
        end
	end

	function fish_prompt
	    set_color $fish_color_cwd
	    printf '%s' (prompt_pwd)
	    set_color normal

	    git_prompt
	    hg_prompt

	    echo ' >: '
	end

This gives you a really clean prompt with just a retro Lost looking `>:` prompt.

There's not really much else I want to write about. This was more of a 'spread the word', evangelical post. Just take the dive, and switch. You won't regret it.

Go and read more in the [fish tutorial](http://fishshell.com/tutorial.html).