---
layout: post
title: 'Festive Mac Screen Saver'
tags:
  - christmas
  - mac

---

Here's a great Mac screen saver to get you in the Christmas spirit--[LotsaSnow](http://wakaba.c3.cx/s/lotsablankers/lotsasnow.html). It generates unique snow flakes that then gently fall down from the top of your screen. It's very customisable and it's a Universal binary.

![LotsaSnow](http://davidwinter.me.uk/wp-content/uploads/2006/12/lotsasnow.png)
