---
category: links
tags: apps
title: Generate animated GIF screencaptures with LICEcap
link: http://www.cockos.com/licecap/
description: Easiest way to capture your screen and generate a .gif
---
