---
layout: post
title: Getting started with Behat
description: A quick introduction on setting up Behat and writing your first test
---

Where [PHPUnit](https://github.com/sebastianbergmann/phpunit) is for testing the individual pieces of your code (unit testing), [Behat](http://behat.org/) is a great tool for testing that an application behaves as expected when following a series of steps (functional testing). And for testing web applications, Behat ties in perfectly with [Mink](https://github.com/behat/mink) to give you a bunch of web browser related tests out-of-the-box.

## Install

We'll be using [composer](http://packagist.org/) to setup Behat and Mink. Create a `composer.json` file in your project directory with:

	{
	    "require": {
	        "behat/behat": ">=2.2.4",
	        "behat/mink": ">=1.3.2"
	    },

	    "repositories": {
	        "behat/mink-deps": { "composer": { "url": "behat.org" } }
	    },

	    "config": {
	        "bin-dir": "bin/"
	    }
	}

If you don't have the `composer.phar` file already on your machine, simply run:

	wget http://getcomposer.org/composer.phar

Then run:

	php composer.phar install

Behat and Mink will then install into a `vendor` and `bin` directory.

In your project directory, run:

	bin/behat --init

This will create a `features` directory for you that contains the required files to get started.

In order to setup Behat to use Mink, go into `features/bootstrap/FeatureContext.php` and make sure that the following is defined:

	require_once __DIR__.'/../../vendor/.composer/autoload.php';

And:

	class FeatureContext extends Behat\Mink\Behat\Context\MinkContext

Here you're including the Mink library, and then ensuring that `FeatureContext` extends from `MinkContext`. You don't really need to worry about what this file does for this example.

Now let Behat know where it can test your web app from. In the root of your project directory, create a file called `behat.yml` and include the following:

	default:
	  context:
	    parameters:
	      base_url: http://localhost/mywebapp/

This is a [YAML](http://en.wikipedia.org/wiki/Yaml) file, and you just need to update the `base_url` value to whatever URL behat can access your app.

You're ready to start.

## Describe your application with features

Run the following command:

	bin/behat -dl

You should see a long list of available definitions that Behat currently has available to it. You can use these to write some powerful feature files that test your application.

First, lets set the scene:

Say on your homepage you have a login form and you want to make sure when someone logs in, they get directed to a dashboard page with a welcome message. You also want to check that when they logout, they are taken to the homepage with a confirmation message saying they've been logged out.

Each test file has the `.feature` extension that Behat uses to search for tests to run when calling the `behat` command. 

These feature files are written in the [Gherkin syntax](http://docs.behat.org/guides/1.gherkin.html). First of all you specify a particular feature you are testing. This is with the keyword `Feature:` and a name. After which, you add a brief description of what the benefit, the role, and the actual feature is. You can have anything here, but it's good practice to follow this format to make things readable.

For each feature, you have a bunch of different scenarios that you want to test against to ensure it matches the expected behaviour. Scenarios consist of definitions that can be prefixed with either **Given**, **When**, **Then** and **And**. These keywords don't mean anything, and are there purely to aid readability. **Given** is where you'd set a bunch of pre-conditions for your test, **When** for the steps taken to test, and **Then** should describe what expected result you should see. **And** can be used to include additional definitions per stage in a scenario.

	Feature: User sessions
		In order to access their account
		As a user
		I need to be able to log into the website
	
		Scenario: Login
			Given I am on "/"
			And I should see "Login"
			When I fill in "email" with "myemail@test.com"
			And I fill in "password" with "mysecurepassword"
			And I press "Login"
			Then I should be on "/dashboard"
			And I should see "Welcome back"
	
		Scenario: Logout
			Given I am on "/dashboard"
			When I click on "Logout"
			Then I should be on "/"
			And I should see "Logged out"

Because we've written the steps in the available Mink definitions, we don't need to write any PHP code! You can now run `bin/behat` in the Terminal and you'll see the result of the tests and whether your application conforms to them--all lit up in green!

These example tests may seem rather trivial, but having them means you have a safety net for your application, so that if you or someone changes something, however small or large to your code, and it breaks the expected application behaviour, your Behat tests will let you know sooner, when it's easy to fix, rather than later when it'll become a nightmare to track down.

As an aside, it should be noted that Behat isn't just great for us developers. If you're working on a project for someone else, you can write up these feature files based on a clients spec, and get them to sign off of them for extra security and peace of mind for all parties.

With the available definitions that Mink provides for Behat, you could quite easily write a ton of tests with just the definitions Mink provides. In a future blog post I'll go on further to show how to write custom defintitions. Hopefully this is enough information to get you started and excited about using Behat in your projects.