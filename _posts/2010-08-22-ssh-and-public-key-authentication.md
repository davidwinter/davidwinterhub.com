---
layout: post
title: 'SSH and public key authentication'
tags:
  - cli
  - howto
  - linux
  - mac
  - osx
  - ssh
  - unix

---

Fed up with having to type your password in each time you log into a server over SSH? Me too. Down with passwords, and in with public key authentication!

First thing to do, is to check that you have a SSH key setup already on your local computer:

    cd ~/.ssh
    ls

If you see some files in there starting with `id_`, like `id_rsa.pub` or `id_tsa.pub`, you're all set. If not, you'll need to generate these files:

    ssh-keygen -t rsa

At the prompts you see, you can just hit enter to accept the defaults.

With your public key now in place, we need to transfer that to the server we want to log into.

    scp ~/.ssh/id_rsa.pub username@remote.server.com:

Don't forget the colon at the end. This has now transfered the public key over. Now you need to log into the remote server (with your password - last time, I promise):

    ssh username@remote.server.com

Now, check if there is a `.ssh` directory on the remote server in your home directory:

    cd ~/.ssh/

If there isn't:

    mkdir ~/.ssh

Now move the public key to a file named `authorized_keys`:

    mv ~/id_rsa.pub ~/.ssh/authorized_keys

Now we need to set the correct permissions to ensure no one can tamper with these files:

    chmod 700 ~/.ssh
    chmod 600 ~/.ssh/authorized_keys

And you're done. Now you can logout, and log back in again - and all going well (if your SSH server has been setup correctly - by default it usually is), you won't be prompted for a password.

If it doesn't work, one problem I encountered was where the actual home directory permissions weren't set to `700`. So try:

    chmod 700 /home/username

All done.
