---
layout: post
title: 'Unix Disk Usage'
tags:
  - command-line
  - dapper
  - freebsd
  - howto
  - ubuntu
  - unix

---

A very handy command to see the total size of a directory on a Unix-based computer.

    du -sh *

This will show the total size of files and directories in your current working directory. The `s` flag means to show a summary--which basically just shows the top level directory only, instead of the contents of each and every folder down the file system hierarchy. The `h` flag shows a human readable file size.

For my home directory, I get the following output:

    404M    Desktop
     30G    Documents
    6.0G    Library
     32G    Movies
     18G    Music
     11G    Pictures
    2.7G    Projects
     16K    Public
    166M    Sites
