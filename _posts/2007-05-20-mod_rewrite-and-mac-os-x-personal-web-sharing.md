---
layout: post
title: 'mod_rewrite and Mac OS X Personal Web Sharing'
tags:
  - apache
  - howto
  - mac
  - wordpress

---

It was very frustrating installing a fresh [Wordpress](http://www.wordpress.org) on my Mac and finding that the permalinks didn't work. Turns out the default Apache settings for personal web sharing on OS X disable `.htaccess` overriding.

    sudo mate /etc/httpd/users/yourusername.conf

Now you'll want to enable Apache to follow symbolic links:

    Options Indexes MultiViews FollowSymLinks

And to allow `.htaccess` overriding:

    AllowOverride All

The file should then look like:

    <Directory "/Users/yourusername/Sites/">
        Options Indexes MultiViews FollowSymLinks
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>

Now in System Preferences, stop and start 'Personal Web Sharing'. Things should work perfectly now.
