---
layout: post
title: 'Watching Freeview (DVB-T) TV with VLC Player on Ubuntu'
tags:
  - freeview
  - howto
  - tv
  - ubuntu
  - vlc

---

Watching TV on my desktop. What do I want to be able to do?

 - Watch all [Freeview](http://www.freeview.co.uk/home) (DVB-T) channels.
 - Be able to pause, rewind, and forward to live TV.
 - Schedule programs to record.
 - See the TV listings for the channels I have.
 - Fullscreen or windowed viewing.

I was able to do all of the above using [EyeTV](http://www.elgato.com/elgato/int/mainmenu/home.en.html) on my Mac. However, I'm trying to make the complete switch to [Ubuntu](http://www.ubuntu.com), and an open-source alternative is needed.

I initially setup [MythTV](http://www.mythtv.org/). It consists of two parts, a server and a client. Multiple clients around the home (or over the internet) can connect to the server. That was a little over-the-top for my needs, but MythTV is a very popular choice for TV on Linux, and does do most of the things I want from the list above, hence my decision to use it.

Once installed, I had TV playing perfectly. Though, when I wanted to start doing some work, while still watching TV, it was a little annoying that I couldn't find anyway of watching the TV in a window--only fullscreen. Trying to find a solution on IRC, some users said there was an option to play TV in a window, however, when I tried to enable the option numerous times, I had no luck.

At this point I thought that my journey to Ubuntu was over. I needed TV on my computer, and if I couldn't watch it in a window while doing other things, then I'd have to keep my Mac around to do that.

I'm not too sure how I stumbled across it, but I found a web page explaining that you could use [VLC Player](http://www.videolan.org/vlc/) to tune into a TV card! And since then, for the last few hours, I've been working on getting that working. At the moment, it only ticks off a few of the boxes, but I'm sure, with a bit more Googling, I'll be able to get the other things functioning. With the following, I can watch all my Freeview channels, in full screen or in a window.

## Setting it all up

Assuming that you have your TV card drivers installed, you can start off with:

    sudo apt-get install dvb-utils vlc

That installs VLC and the DVB programs needed to scan for your channels.

Now, find the digital transmitter nearest to you by visiting [DigitalUK](http://www.digitaluk.co.uk/postcodechecker/). Enter in your postcode, and the result you get back will tell you which transmitter you're feeding off of. If you're not in the UK, I'm not sure how you can find your closest one--[Google](http://google.com) is your friend.

You can see a list of the UK transmitters by doing:

    ls /usr/share/doc/dvb-utils/examples/scan/dvb-t/ | grep uk-
    
Now find the filename that matches your transmitter, and substitute `uk-CrystalPalace` for it.

    scan /usr/share/doc/dvb-utils/examples/scan/dvb-t/uk-CrystalPalace -o zap | tee ~/channels.conf

The above command will scan for channels that your TV card can pick-up, and store the configuration in a file. This file VLC then uses to tune in.

Now open up VLC. **File** > **Quick Open File**. Browse to `channels.conf` and open. Live TV should now start playing. To see a list of channels to switch between, go to **View** > **Playlist**. 

<a href="/wp-content/uploads/2008/02/tv-vlc.png" rel="lightbox" title="Watching TopGear from Freeview in VLC player" class="image_link"><img src="/wp-content/uploads/2008/02/tv-vlc.thumbnail.png" alt="VLC Player" /></a>

To speed things up when wanting to launch the TV each time, you can create a launcher. I'll create it on the Desktop for examples sake. Right click, and select **Create Launcher**. Give it a name, like TV, and in the Command text box, enter `vlc /path/to/channels.conf`. Hit ok, and you're done. Now double click on the launcher and VLC will launch, and you'll have the TV back on.

That's it! Now all that is left for me to do, is to see if there is a way I can get the other things on my list operational; pause, schedule, tv listings. I'll report back on here with any developments I find. Tune in again next week for more exciting adventures with Ubuntu!

What TV app do you use on Ubuntu?
