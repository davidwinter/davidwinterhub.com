---
layout: post
title: 'Using o2 PAYG mobile broadband'
tags:
  - howto
  - internet
  - mac
  - o2
  - osx

---

Our internet connection via our landline has been dead since Tuesday afternoon, so I've needed an alternative connection in the meantime to give me my twitter fix! 

I'd kept an eye on the o2 pay-as-you-go mobile broadband for a while, because it offered the cheapest, noncommittal setup. A one off £20 for the USB dongle, and then as little as £2 for 500MB for a 24 hour period. 

So I plug in the dongle and we get a nice little installer to set everything up; installing drivers and the o2 Mobile Connect application. The annoying thing is that if you install going this route, you can only connect if you have this app open. You can't just use system preferences. Also, turns out this 'handy' installer program then blocks you from getting to the installation files again, and the dongle will refuse to do anything unless Mobile Connect is open (you'll get a red light on the dongle when it's being evil).

So, what can you do?

 1. Drag `Mobile Connect.app` to the trash from your Applications directory. 
 2. Then head into System > Library > Extensions and delete `ZTEUSBCDCACMData.kext` and `ZTEUSBMassStorageFilter.kext`
 3. Go into Network in System Preferences and remove the devices starting with ZTEUSB.
 4. Then restart your Mac.

Now, let's install things a different route giving everyone more flexibility.

 1. Plug in the dongle and when the CD Icon appears on the desktop, open it up, but rather than double clicking on the installer, right click on it and select 'Show Package Contents'. 
 2. Now head to Contents then Resources. Now double click on the `drv.pkg` file and install this only. 
 3. Once complete, restart your Mac again. 

And now to configure the connection:

 1. Open System Preferences, and go to the Network pane. 
 2. You should have three new items in there. Click on ZTEUSBModem. 
 3. Set the telephone number to `*99#`
 4. Username to `o2bb`
 5. Password to `password` 
 6. Click on Advanced. Select Vendor; Generic, Model; GPRS (GSM/3G) and set the APN to `m-bb.o2.co.uk` 
 7. Click on OK and then Apply. 
 6. Now you can click 'Connect' and all should work fine. 

That is of course, besides the crappy image compression and caching o2 force onto you! Need to find a solution for that next.

**Update**: Here's a solution for getting around the image compression, and Javascript injection; use a SSH SOCKS proxy. Works a treat.

 1. Open up `Terminal.app`
 2. Run `ssh -ND 9999 username@yourserver.com`
 3. Enter your password if/when prompted (you may have key authentication setup)
 4. That's the SOCKS proxy setup. Now go to System Preferences and open the network connection.
 5. Click on ZTEUSBModem.
 6. Then Advanced, Proxies
 7. Check the SOCKS Proxy and enter in `localhost` and `9999`
 8. Click OK and then Apply.

All done. You should now get perfect images and no longer have Javascript inserted into all webpages. Both of which weren't clearly mentioned when purchasing my dongle.
