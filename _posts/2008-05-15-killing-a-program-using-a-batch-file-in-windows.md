---
layout: post
title: 'Killing a program using a batch file in Windows'
tags:
  - batch-file
  - command-prompt
  - windows

---

Having got my [keyboard shortcut working](/articles/2008/05/15/using-shortcut-keys-in-windows/) to launch my batch file, next was to actually make it quit the emulator program when invoked.

    @echo off
    taskkill /F /im Fusion.exe

This is basically the command prompt version of <kbd>CTRL + ALT + DEL</kbd>. `/im` stands for 'Image Name', which is basically the program executable, and is the same as what you'd see in the 'Task Manager' window. `/F` just forces the program to close.
