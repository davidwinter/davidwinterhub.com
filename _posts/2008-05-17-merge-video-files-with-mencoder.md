---
layout: post
title: 'Merge video files with mencoder'
tags:
  - cli
  - mac
  - mencoder
  - ubuntu
  - unix

---

To merge video files together with `mencoder` is simple:

    mencoder -oac copy -ovc copy file1.avi file2.avi file3.avi -o full_movie.avi
