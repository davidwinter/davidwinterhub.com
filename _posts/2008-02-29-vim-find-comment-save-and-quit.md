---
layout: post
title: 'vim: find, comment, save and quit'
tags:
  - gvim
  - tips
  - vim

---

For the last month, I've been teaching myself `vim`. So here's an explanation for anyone interested in the commands I used for [this post](http://davidwinter.me.uk/articles/2008/02/29/remove-floppy-icon-from-gnome/).

    /fd0
    I
    #
    <esc>
    :wq

Upon opening the file, I used the `/` command to search for 'fd0' in the file. In command mode, you just type a forward slash, and then the characters to search for straight after it. Hit enter, and it'll give you the first result.

Next up, I want to insert a comment on that line. Because searching left me a few characters from the start of the line--where I want to have the comment--I enter _insert_ mode with a capital `I`. This puts the cursor at the start of the current line. I then put in my `#` to comment the line, hit escape to get me back to command mode and then saved and quit the file with `:wq`.

I'm finding vim a lot friendlier to work with than emacs. It's not quite TextMate yet, but I'm finding it rewarding learning all these different commands.
