---
layout: post
title: 'PHP development on Mac OS X 10.6 Snow Leopard'
tags:
  - apache
  - cli
  - mac
  - macosx
  - php

---

Having a reliable and easy to use developer environment on my Mac is essential for my work, and the odd project I work on. 

I original used the [Entropy PHP packages](http://www.entropy.ch/software/macosx/php/) as they were so easy to install, and _just worked_. Unfortunately, Marc Liyanage had stopped updating the packages with new OS X releases, so I then tried alternatives such as [Macports](http://davidwinter.me/articles/2010/01/02/apache-php-and-mongodb-on-mac-os-x-10-6-snow-leopard/), and more recently [MAMP](http://www.mamp.info/). However, it hasn't been working out of late, so I went in search of alternatives.

For a long shot, I visited Marc's PHP page and noticed that he is now [endorsing a project](http://php-osx.liip.ch/) which forked off from his own. Like his packages, it uses the default Apache install that comes with OS X, but uses their own alternative PHP package. It installs into `/usr/local` so that it doesn't interfere with anything else. So here are some steps to get this all up and running.

## Setup

Before I started, I uninstalled Macports and MAMP. Not required, but I didn't want any of those packages to interfere or conflict with the new install of PHP. I now use [homebrew](http://mxcl.github.com/homebrew/) as a replacement to Macports for tools that I require (such as MongoDB and MySQL).

### Install PHP for OS X

Install PHP via [http://php-osx.liip.ch](http://php-osx.liip.ch):

	curl -s http://php-osx.liip.ch/install.sh | bash -

It'll ask for your password so that it can install to `/usr/local`. If you're cautious of running a shell script from a webpage--as you should be!--why not view the source in your browser first to be sure all is as it should.

### Setup path

If you want to use any of the PHP tools via the command line, such as `pear` or `pecl`, it's best if you add the new PHP `bin` to your path. I updated mine to:

	export PATH="/usr/local/php5/bin:/usr/local/bin:$PATH"
	
The important part here is the inclusion of `/usr/local/php5/bin`. You'll then want to open a new terminal window so that the path update takes effect. Check by running:

	echo $PATH

If you're going to use `pear` or `pecl`, it may be worth checking that it's configured to use the correct `php.ini` file.

Run the following command:

	pear config-get php_ini

If it doesn't say:

	/usr/local/php5/lib/php.ini
	
Then run:

	sudo pear config-set php_ini /usr/local/php5/lib/php.ini

And then just check that `pecl` has the same config set:

	pecl config-get php_ini

That should now be PHP setup and running.

## Easy Apache configuration

With some great pointers by [Roger Johansson](http://www.456bereastreet.com/archive/201104/apache_with_virtual_hosts_php_and_ssi_on_mac_os_x_106/) here is a great way to make configuring Apache easy for projects you work on. You'll need to edit two files. One will make some hostname aliases to your local machine so that you can access a project via a url such as `http://myproject.local`. The other file is the Apache configuration file for your user, where you specify the server name and document root location. Here we go:

Edit `/etc/apache2/users/yourusername.conf` substituting `yourusername` with the actual username you use on your Mac. Update it so that it looks like this (again updating the path with your actual username):

	NameVirtualHost *:80

	<Directory "/Users/yourusername/Sites/">
	    Options Indexes MultiViews FollowSymLinks Includes
	    AllowOverride All
	    Order allow,deny
	    Allow from all
	</Directory>

	<VirtualHost *:80>
	    ServerName myproject.local
	    DocumentRoot /Users/yourusername/Sites/myproject.local
	</VirtualHost>

Now edit `/etc/hosts` and add to the bottom:

	127.0.0.1    myproject.local
	
Now reload Apache, and you should be all set:

	sudo apachectl graceful

You're all set now! Hopefully this will make PHP development easier for you, as it has done for me.
