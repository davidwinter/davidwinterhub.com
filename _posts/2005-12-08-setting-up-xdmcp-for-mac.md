---
layout: post
title: 'Setting up XDMCP for Mac'
tags:
  - howto
  - mac
  - ubuntu

---

I just finished setting up a spare PC I had with [Ubuntu](http://ubuntulinux.org "Ubuntu") on it. Thing is, it's damn noisy so I want it stuffed up in the loft out of the way. This is fine because I can remote log-in to it using VNC, right? The answer would be yes if I made sure I was actually logged into my account on the machine - but that would mean having to get a monitor/keyboard/mouse up there to do so.

Alternative? XDMCP. It's kind of like VNC, but allows you to connect to your machine if no one is logged in. I don't know the technical what's going on in the background, but I've managed to get this working on my Mac.

### Enable XDMCP on Ubuntu

First of all you'll need to enable XDMCP access on the Ubuntu machine. This is really simple to do:

1. `System` > `Administration` > `Login Screen Setup`
![Menu](/images/articles/xdmcp-for-mac/login-screen-setup-menu.png "Login Screen Setup Menu")
2. Enter your administration password
![Enter Password](/images/articles/xdmcp-for-mac/password-prompt.png "Password prompt")
3. Under the Security tab, `Enable XDMCP`
![Enable XDMCP](/images/articles/xdmcp-for-mac/enable-xdmcp.png "Enable XDMCP")
4. You can then `Close` the window

### X11 on Mac OS X

When I upgraded to [Tiger](http://www.apple.com/macosx/ "Mac OS X Tiger"), I didn't install [X11](http://www.apple.com/macosx/features/x11/ "X11") which you'll need to pull this all off. Fear not - it's easy to install. 

1. Just slide in your Tiger install CD. 
2. When it's in, scroll down the Finder window slightly and you'll see Optional Packages or something similar. 
3. Run that app and install X11.

### Connecting

Once X11 is installed, and XDMCP is enabled on Ubuntu, all that's left is to actually connect.

1. Open `Terminal.app`
2. Enter in `/usr/X11R6/bin/X -query XXX.XXX.XXX.XX` where XXX.XXX.XXX.XXX is the IP of the Ubuntu machine
3. A new app will open named `Xquartz` and a few seconds after you should see the Ubuntu login screen appear.

And that's all there is to it! You're then able to login and work on Ubuntu as if you were actually sitting in front of it. Expose even works which is really handy.

The only problem I've had is that it's sometimes hard to switch between windows. If you're having problems, just click on the `Xquartz.app` icon in the dock to get Ubuntu back.

When you're finished, log out in Ubuntu and then switch to `Terminal.app` in OS X and hit `CONTROL + C` to quit `Xquartz.app`.
