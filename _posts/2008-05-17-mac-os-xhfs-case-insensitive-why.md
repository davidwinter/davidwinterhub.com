---
layout: post
title: 'Mac OS X/HFS+ case-insensitive? Why?'
tags:
  - mac
  - osx
  - rant

---

I just found out that HFS+, the preferred file system for Mac OS X, is case-insensitive when it comes to files and directories. I don't understand why? It's sure as hell causing issues with stuff I'm copying from my Ubuntu PC.

Just seems like a dumb idea.
