---
layout: post
title: Setup rbenv on Mac OS X
description: Some quick setup instructions on getting rbenv installed on Lion
---

[rbenv](https://github.com/sstephenson/rbenv) is a quick way to get different ruby versions installed on your OS, without interfering with any default installs it may provide.

Run the following commands:

	brew update
	brew install rbenv
	brew install ruby-build

In your bash profile file, ensure that the following is included:

	eval "$(rbenv init -)"

Now, install the latest version of ruby:

	rbenv install 1.9.3-p0

Run the following to set `rbenv` to use this new version of ruby.

	rbenv global 1.9.3-p0

Open a new shell and type `ruby --version` and if you see `ruby 1.9.3p0 (2011-10-30 revision 33570) [x86_64-darwin11.2.0]` then all is good and you're all setup!

One thing to note is that if you install a new gem that installs a binary, you'll need to run `rbenv rehash` afterwards in order for the binary to become available to you on the command line.