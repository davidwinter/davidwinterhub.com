---
layout: post
title: 'A simple PHP delicious REST example with pecl_http'
tags:
  - code
  - delicious
  - example
  - http
  - pecl_http
  - php
  - rest

---

    <?php

    $username = 'your_username';
    $password = 'your_password';
    $url = "https://$username:$password@api.del.icio.us/v1/posts/recent";

    $request = new HttpRequest($url, HTTP_METH_GET);
    $response = $request->send();
    print $response->getBody();
    ?>

You'll probably need to view the source in your browser to see the output.

The second argument isn't required in this call, because `HttpRequest` defaults to a `GET` request anyway, but it's just helpful to see where you can specify the different HTTP methods. The others being, post, put and delete.
