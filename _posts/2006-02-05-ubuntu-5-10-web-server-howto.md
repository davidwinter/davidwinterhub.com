---
layout: post
title: 'Ubuntu 5.10 web server howto'
tags:
  - apache
  - howto
  - mysql
  - rails
  - ruby
  - ubuntu

---

I reinstalled my home server today (well, I actually started yesterday, but today I finished from scratch again) and wrote down what I did. My aim was to have a web server set up using Apache that I could use to host this blog which uses Ruby on Rails. Also, I wanted to set-up Webmin and Bind so I could manage the DNS stuff from here.

So, here we go! If you find any errors, or have any problems, leave a comment and I'll try and help.

### The headache begins

I'm assuming that you've just completed a new default install of Ubuntu 5.10.

As my space PC/server is up in the loft, and it's freezing up there, the first thing I did was to enable XDMCP on Ubuntu so I could connect to it from my Mac downstairs in my nice warm bedroom. You can read how to do that [here](http://davidwinter.me.uk/articles/2005/12/08/xdmcp-for-mac "Setting up XDMCP for Mac"). 

With XDMCP now running, before I can connect to it via my Mac, I set the IP address of the machine to 192.168.0.1 - a nice number that's easy to remember.

Back in my room I'm connected to Ubuntu via XDMCP and it's as if I'm sitting up in the loft.

If you're not using XDMCP (and even if you are you should follow this step), you'll want to connect via SSH. Install it using:

    sudo apt-get install ssh

There are some packages that are needed that aren't in the default repositories for `apt`, so you have to enable them manually. Universe and Multiverse are needed.

To enable the Universe servers, I did

    sudo nano /etc/apt/sources.list

Inside that file, uncomment the two lines that look something like this:

    deb http://gb.archive.ubuntu.com/ubuntu breezy universe
    deb-src http://gb.archive.ubuntu.com/ubuntu breezy universe

Hit Control O to save and press enter to use the same filename. Then press Control X to close Nano.

I could probably enable the Multiverse stuff in there too, but as I've not tested it like that, I'll leave it to exactly how I did it.

Open up the Synaptic Package Manager
1. Settings
2. Repositories
3. Add
4. Check Multiverse
5. Click OK and let it rescan the servers.

You can then close Synaptic. `apt` is now all set up ready for installing the packages we need.

Before we start installing the packages we want, do a system update:

    sudo apt-get update

#### Ruby

All on one line:

    sudo apt-get install irb libdbm-ruby1.8 libfcgi-ruby1.8 libfcgi0 libgdbm-ruby1.8 libmysql-ruby1.8 libmysqlclient12 libopenssl-ruby1.8 libruby1.8-dbg mysql-common ri ri1.8 ruby1.8-dev ruby

#### MySQL Server

    sudo apt-get install mysql-server

During this install, a Postfix config screen will appear. Select OK and then "No configuration".

#### Ruby Gems & Rails

This is the standard way of installing ruby applications.

Need to download it directly from its web site:

    wget http://rubyforge.org/frs/download.php/5207/rubygems-0.8.11.tgz

Then extract it:

    tar xvfz rubygems-0.8.11.tgz

Move into the extracted directory:

    cd rubygems-0.8.11

Run the setu-p program:

    sudo ruby1.8 setup.rb

Update all installed gems on the system:

    sudo gem update --system

Now that Ruby Gems is installed, we can install Rails.

    sudo gem install rails -y

#### Apache 2

Next is the web server I'm using. Apache 2. The following command will install Apache 2, FastCGI, FCGI, PHP 5 with MySQL functionality. PHP isn't needed for Ruby or Rails, but I'm including it so that I can run PHP apps...

    sudo apt-get install apache2 libapache2-mod-fcgid libapache2-mod-fastcgi libapache2-mod-php5 php5-mysql

Once that's done, we'll need to enable the Apache mods we'll be using:

    sudo a2enmod fcgid
    sudo a2enmod fastcgi
    sudo a2enmod rewrite

You'll notice I've installed two different types of Fast CGI. That's because at some point I want to try them both out. However, at the moment, I've only had success with getting FastCGI working decently.

Restart Apache to make sure all is well.

    sudo /etc/init.d/apache2 restart

#### FastCGI

I thought FastCGI would have been installed after doing the libapache2-mod-fastcgi - but for some reason, you have to install it manually.

In order to configure and build from the source files, you need to download some basic files:

    sudo apt-get install build-essential

Then, grab the FastCGI files from the web site:

    wget http://fastcgi.com/dist/fcgi-2.4.0.tar.gz

Extract:

    tar xvfz fcgi-2.4.0.tar.gz

Move into the directory:

    cd fcgi-2.4.0

Configure the installer:

    ./configure

Install:

    sudo make install

Now, that is all of the Apache and FastCGI files installed. Later, we'll need to configure it all.

#### Webmin

Webmin is ugly, but it works, and it lets us configure Apache and the Nameserver easily.

Go to http://www.webmin.com/ and download the latest version of webmin in tar.gz format.

Once downloaded, extract it (filename may vary depending on the version you download):

    tar xvfz webmin-1.260.tar.gz

Move into the directory:

    cd webmin-1.260

Run the set-up program:

    sudo ./setup.sh /usr/local/webmin

You can use the default settings for this, or change them if you know what you're doing. Be sure to set a password you can remember.

At the end of the set-up you'll be given an address to use to connect to webmin. Remember this! and go to it straight away and login.

Do the following to configure Webmin to work with Apache:
1. At the top, click on the "Servers" link.
2. Select Apache Webserver.
3. Select Module Config.
4. Set the following values:
    * Apache Server Root - /etc/apache2
    * Path to httpd executable - /usr/sbin/apache2
    * Path to httpd.conf - /etc/apache2/httpd.conf
    * Command to start Apache - /etc/init.d/apache2 start
    * Command to stop Apache - /etc/init.d/apache2 stop

Save these settings.

#### Nameserver

I'm no DNS expert, so this is very vague and I suggest you go read a howto on DNS... [here's one](http://rimuhosting.com/support/bindviawebmin.jsp) I used to complete this part.

This will be different depending on your set-ups. For me, I have my domain name pointing to my web server in America that hosts http://commanderbond.net 

On the server, there, I have the DNS pointing to my Internet IP at home, here in the UK.

The nameserver I'm using is Bind.

    sudo apt-get install bind9

Once that's installed, we need to configure it in Webmin.
1. Click the "Servers" link.
2. Select BIND DNS Server.
3. A message will appear about a configuration file. Select the 3rd option "Setup as an internet name server, but use Webmin's older root server information"
4. Create a Master Zone and enter the following details:
    * Domain Name - yourdomain.com
    * Email - you@anemail.com
5. Click on "Create".
6. Click on the "Address" link.
7. Create a new Address record using the following:
    * Name - yourdomain.com. (including the trailing dot)
    * Address - your Internet IP address
8. Click on "Create".
9. Go to the Module Index.
10. Click on "Apply Changes".

#### Set-up your web site

You'll need somewhere to store your web site files, so I suggest to first of all create a `public_html` directory in your user directory.

    mkdir ~/public_html

Now we'll create a test HTML file to check all is working so far.

    nano ~/public_html/index.html

Inside it put the following:

    <p>Hopefully you can see this...</p>

Save the file using Control O and then Control X to quit Nano.

Now, set the permissions of the file so it's visible to the world:

    chmod 644 ~/public_html/index.html

In Webmin, go to the Apache configuration.

Create a new Virtual Server with the following:
* Document Root - home/yourusername/public_html
* Server Name - yourdomain.com

Save the settings. Now restart Apache:

    sudo /etc/init.d/apache2 restart

Now visit http://yourdomain.com/~yourusername/ and you should see:

> Hopefully you can see this...

If so, excellent! If not, hmmm... maybe you didn't follow everything? or I made a mistake somewhere... leave a comment below.

Take a 5 minute break!

Refreshed? Right. Moving on...

#### phpMyAdmin for MySQL management

Go to http://www.phpmyadmin.net/home_page/index.php and download the latest stable release of phpMyAdmin in tar.gz format.

Extract:

    tar xvfz phpMyAdmin-2.7.0-pl2.tar.gz

Move the directory to some place helpful:

    mv phpMyAdmin-2.7.0-pl2 ~/phpmyadmin

Move into that directory:

    cd ~/phpmyadmin/

Create a new configuration file:

    cp config.default.php config.inc.php

Change the authentication mode to http

    gedit config.inc.php

Change this line:

    $cfg['Servers'][$i]['auth_type'] = 'config';

...to this line:

    $cfg['Servers'][$i]['auth_type'] = 'http';

Save the file.

Back in the command line, we need to set the root password, because by default, there isn't one! 

    mysql -u root

You'll then enter the MySQL console. Enter the following:

    UPDATE mysql.user SET Password=PASSWORD('newpasswordhere') WHERE User='root';

This will set the root password. Now flush the privileges:

    FLUSH PRIVILEGES;

Now exit:

    exit

Now, we want phpMyAdmin accessible on the server 'globally'. So that it doesn't matter what address we're using, we'll still be able to access it.

In Webmin, under Apache Webserver:
1. Select "Default Server"
2. Aliases and Redirects
3. Under "Document directory aliases" enter `/phpmyadmin/` into the first text box, and `/home/davidwinter/phpmyadmin/` into the box to the right of that one.

Save, and in the Terminal restart apache:

    sudo /etc/init.d/apache2 restart

Go to http://yourdomain.com/phpmyadmin/

Login in using "root" and the password you set in the MySQL console.

You can do any MySQL configuration here. For me, I restored my Typo database for this blog from a backup.

#### FastCGI configuration with a Rails application

This part is what gets a Rails app running really fast.

You'll need to configure the FastCGI config file:

    sudo nano /etc/apache2/mods-enabled/fastcgi.conf

Here is my config file:

    <IfModule mod_fastcgi.c>
      #FastCgiWrapper /usr/lib/apache2/suexec2
      FastCgiIpcDir /var/lib/apache2/fastcgi
      FastCgiConfig -maxClassProcesses 2 -maxProcesses 2 -minProcesses 2 -processSlack 2
      FastCgiServer /home/davidwinter/typo/public/dispatch.fcgi -idle-timeout 120 -processes 2 -initial-env RAILS_ENV=production
    </IfModule>

The following line of the above:

    FastCgiServer /home/davidwinter/typo/public/dispatch.fcgi -idle-timeout 120 -processes 2 -initial-env RAILS_ENV=production

Is important. You need to specifiy a line like this for each Rails app you'll be using under FastCGI.

Now, in your Rails application directory check that in `public/.htaccess` has this line:

    AddHandler fastcgi-script .fcgi

And, that in the rewrite rules section, it has this:

    RewriteRule ^(.*)$ dispatch.fcgi [QSA,L]

...instead of:

    RewriteRule ^(.*)$ dispatch.cgi [QSA,L]

Notice `.fcgi` instead of `.cgi`

And that's about it!

Give Apache one final restart:

    sudo /etc/init.d/apache2 restart

And you should now have everything working.

As I said, any problems, leave a comment and I'll try and help.
