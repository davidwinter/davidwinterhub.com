---
layout: post
title: 'Remove startup scripts on Ubuntu'
tags:
  - cli
  - ubuntu
  - unix

---

    sudo update-rc.d -f script_name remove
