---
layout: post
title: 'Enable PHP error logging'
tags:
  - php
  - ubuntu

---

In php.ini:

    display_errors = Off
    log_errors = On
    error_log = /var/log/php-errors.log

Make the log file, and writable by www-data:

    sudo touch /var/log/php-errors.log
    sudo chown www-data:www-data /var/log/php-errors.log
