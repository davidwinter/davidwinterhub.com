---
layout: post
title: Setting up Github Pages
description: Getting started with a Jekyll powered site and migrating from WordPress
---

I've not had time recently to keep my copy of [WordPress](http://wordpress.org) up-to-date (yes, even though it's [setup with SVN](http://codex.wordpress.org/Installing/Updating_WordPress_with_Subversion)). I wanted something a little easier to manage. Having read good things about [Github Pages](http://pages.github.com/), I decided to take the plunge. I've now migrated this entire site over. It's simple to setup, and also has [Jekyll](https://github.com/mojombo/jekyll/) support enabled by default.

Jekyll allows you to generate a static website from plain text post files, and templates. When you push your new posts to Github, Jekyll is run on the files and the static site is built and hosted for you.

## Setup

To get started, install `jekyll`:

	sudo gem install jekyll

If you want to use [Markdown](http://daringfireball.net/projects/markdown/) formatting in your posts, also install:

	sudo gem install rdiscount

Now you'll need to create a git repository to manage your blog. There is a great [Jekyll skeleton setup](https://github.com/danielmcgraw/Jekyll-Base) that I used here, so we'll go ahead and clone that.

	git clone https://github.com/danielmcgraw/Jekyll-Base.git yourgithubusername.github.com

Be sure you replace `yourgithubusername` with your Github username. This isn't required on your local repo, but is for the repository you create on Github. It's in this format so that Github can detect it's a Github Pages site, and so it just makes sense to name it the same locally too.

Now, we don't want the git history for this skeleton setup, so remove the `.git` directory:

	cd yourgithubusername.github.com/
	rm -rf .git

To test things are working at this stage, open another terminal window in the same directory and run `jekyll --server`. It keeps a process open, along with a webserver running on port 4000, monitoring file changes, and rebuilding the site automatically on the fly. So you'll want to keep this terminal window open and running. You should now be able to see the generated site by visiting [http://localhost:4000](http://localhost:4000)

When `jekyll` builds the site, it puts the generated files into a `_site` directory. You don't want this directory in Git, so in the `.gitignore` file add `_site` to the bottom so that it'll be ignored.

Now on Github, create a new repository naming it in the same format `yourgithubusername.github.com`. Then on your local machine:

	git init
	git add .
	git commit -m "Initial commit. Jekyll base setup."

Then follow Github's instructions on adding the remote reference, which is along the lines of (substitute `username`):

	git remote add origin git@github.com:username/username.github.com.git
	git push -u origin master

This pushes your site up to Github. After this initial push you can do a plain old `git push` when you've made changes that are ready to be published. 

They say it can take up to 10 minutes for new sites to be generated when they are first pushed, however, it'll likely be under a minute. They'll email you either way when it's done.

You can then visit [http://yourgithubusername.github.com](http://yourgithubusername.github.com) to see your new static site all built, and being served. Snappy isn't it?

## WordPress migration

There were two important things that I wanted to ensure kept the same during the migration to Github pages; the URLs for the posts, and the comments posted.

My permalink structure on WordPress was:

	/articles/%year%/%monthnum%/%day%/%postname%/

So to set this up on Jekyll, all I had to do was edit `_config.yml` and set:

	permalink: /articles/:year/:month/:day/:title/

For comments, I had already been using [Disqus](http://disqus.com/) on my WordPress install, so they already had that content. All I had to do was setup the Javascript to pull in those comments.  In the `_layouts/post.html` file, all I did was add:
	
	<div id="disqus_thread"></div>
	<script type="text/javascript">
	    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
	    var disqus_shortname = 'davidwinter'; // required: replace example with your forum shortname
	    var disqus_url = 'http://davidwinter.me{% raw %}{{ page.url }}{% endraw %}';

	    /* * * DON'T EDIT BELOW THIS LINE * * */
	    (function() {
	        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
	        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
	        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
	    })();
	</script>
	<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>

The important stuff here is the `disqus_shortname` and `disqus_url` variables. These are required in order for Disqus to know which site comments to display, and the `disqus_url` to know which page comments to render. Because my URLs are staying the same, this was easy by just using the domain and the `{% raw %}{{ page.url }}{% endraw %}` variable.

While you're setting this up locally, you may want to include:

	var disqus_developer = 1;

As the [help documents](http://docs.disqus.com/help/2/) explain:

 > Tells the Disqus service that you are testing the system on an inaccessible website, e.g. secured staging server or a local environment. If disqus_developer is off or undefined, Disqus' default behavior will be to attempt to read the location of your page and validate the URL. If unsuccessful, Disqus will not load. Use this variable to get around this restriction while you are testing on an inaccessible website.

Be sure that this is only set while testing the integration of Disqus. When you know it's working locally, remove this variable, so that it doesn't get pushed up to Github.

And also, ensure that your `yourgithubusername.github.com` address is added to your trusted domains list in the Disqus admin.

One last thing that is quite important to bring over to my new Github Pages setup is all of my old posts... duh. Depending on the size of your existing blog, you may just want to do this by hand. However, with 50+ posts, I wanted something automated.

In WordPress, you can do an export of your posts under the Tools section of the left hand menu. Just select to export the posts, not all content, as you don't need it.

I wrote a [quick PHP tool](https://github.com/davidwinter/wordpress-to-jekyll) to convert this exported data into Jekyll friendly post files. Once these have been generated, you can copy them into your `_posts` directory, and Jekyll will generate the pages (if it's still running).

Push up to Github, and you're all set. Just tinker around with the layouts and styling and you'll then have a super fast, easy to manage, Jekyll powered Github Pages site.
