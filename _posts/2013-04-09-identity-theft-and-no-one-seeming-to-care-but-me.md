---
layout: post
title: Identity theft and no one seeming to care but me
description: Maybe don't waste your time reporting it in future...
---

_This post is a little out of the ordinary for my blog - well, OK, quite a bit. But I thought it was worth posting in the hope that other people who have had this happen to them can get in touch. If you're expecting another tech related post, don't bother to continue reading!_

---

I woke up last Thursday to a letter in the post from Vodafone. Strange I thought, seeing as though I had cancelled my contract with them ages ago and was now on a new network. 

The letter was a confirmation of setting up a direct debit. It had my name and address (obviously). The bank account used for the direct debit was in my name, however the sort code and account number weren't mine. So that was a relief right away knowing that my account hadn't been drained. 

Someone had ordered a phone using my name and address, and either setup a bank account in my name, used some other poor persons or just faked the details.

I did a search for the bank via the sort code which resulted in a Derby branch of Lloyds TSB. I definitely don't have a Lloyds account and don't think I've ever been to Derby.

Visited the Vodafone website and saw that they had online live chat. Not had much success with these previously--usually someone in an office reading off a script--never usually helpful. I let the support person know the situation. The letter had on it the account number of this new account so I gave them that. They were able to block the account and the phone within a few minutes.

They were also able to tell me that the phone had been bought in a shopping centre local to where I live. _Interesting._ Not just some random scammer in a far away town, but actually someone in close proximity to me. I straight away thought someone had been through my rubbish and got my details from that, but then reflected thinking how paranoid I am about these sorts of things, and therefore shred 99.9% of all post I receive. There was a possibility someone had found something of mine, but a low one I think. So then my mind wanders to think perhaps it is someone I know. Lots of people know my address, I have a few 'enemies', and considering how close the shopping centre is to me that the phone was bought at, I think that is a higher probability.

I asked the live chat person how someone can buy a phone in a retail store. They said they'd need some sort of ID with them. I asked whether that may be photo ID and they said yes. The stores make a copy of these when the purchase is made. So if someone had used photo ID it'd have been with their face and my name on it.

The support person suggested I go to the store and talk to the Manager there to see the ID, beacuse I said it may be someone I know. Also the store would most likely have CCTV and I had the time and date the purchase was made. Surely a simple case of reviewing the CCTV footage and asking me to see if I recognised the person.

The support person (Shakib K) was so very helpful. I was surprised---but grateful---that I was able to contact someone so quickly and painlessly. The Vodafone account had been blocked and the support person had submitted an internal fraud report to Vodafone that would be investigated, and I passed on my details so they could contact me.

Before heading to the Vodafone store, I tweeted Lloyds TSB asking them who I should contact about the fraud. The first reply I had just said I needed to go into a branch. Unhelpful and not convienient considering I'm trying to help them and their customer. I replied back asking if there was a telephone number I could contact. I got another reply with a number to call, which I did. 

I explained the situation, gave them the sort code and account number. Interestingly, the name they had for the account wasn't mine, but someone elses. So someone hadn't created a fake account of mine, but was instead using someone elses. But how the hell was Vodafone and Lloyds TSB able to create the direct debit if the names of the account holder didn't match? Surely something is wrong with that process. Sounds so easy for the criminal to do.

When I suggested to them that they should contact the account holder to make them aware that someone may be using their bank account to steal money, they said they coudln't do anything. Nothing at all. Zip. 

Ridiculous.

I suggested that they should see if a Vodafone direct debit existed on the account and should cancel it. I had the date the confirmation had been made. 

Nope, couldn't do that either. They said I had to go into a branch and report it.

What the hell? You have someone calling them up, at what I assume was their fraud department, or something similar, explaining that a dodgy direct debit had been setup on an account of theirs, and they don't do anything? No action would be taken.

They expect the person reporting it to go out of their way, waste their time reporting it? Yes, I wouldn't have minded, but after the unhelpful responses on twitter and on the phone, I decided against it. I had pleaded with them to at least flag the account. But they wouldn't.

I understand they can't delete the direct debit just because I called up. I understand they can't just phone the customer and say someone has phoned up saying money has been taken out of their account. But they could at least add a note to the account so that if the account holder does call up, they have some information or clue as to why money has been stolen from them. They could at least write a letter saying that they suspect someone has setup a fraudulent direct debit. The account holder could at least then check to see if that is the case, and call them to get assistance, or ignore it if nothing of the sort has happened.

Wouldn't you like your bank to tell you as soon as possible if their is something suspicious? Or someone has reported something? I sure as hell would.

Lloyds TSB didn't seem to be concerned at all, and weren't bothered about their customer. They obviously prefer to be reactive rather than proactive in supporting their customers. I certainly won't bank with them, or recommend them to anyone in the future.

So after that useless help from Lloyds TSB, I made my way to the Vodafone store. I took the letter with me, took my passport and a utility bill so that I had some genuine proof of ID with me.

I got to the store and had to wait around 10 minutes to see the manager. I explained the situation, and said that the online chat Vodafone rep had suggested I come to the store. I offered to show them the letter but the manager didn't seem that interested. Because I had told them that no money had been stolen from me personally, that they couldn't show me the ID used to make the purchase. Even though this was someone who had impersonated me and I had the letter to prove it. I was one of the victims.

I had had such excellent support from the online chat staff, but the feeling I got from the manager was that he wasn't bothered at all. He obviously seems to prefer spending more time and care for people impersonating and stealing from others, than someone trying to report the situation and help apprehend and prosecute the criminal. Crazy.

The Police station was very close to the shopping centre, so I decided to go their to report the incident. I was aware that there was an online form to do the same thing, but because the shopping centre was so close to me, I thought the local police might be interested as it could possibly be someone I know. I ended up waiting 30 minutes and the desk clerk telling me to report it online. Dead end there. More time wasted.

Disillusioned, I head back home and look at the online fraud reporting form. At the very start, it says that the submission may not result in the crime being investigated. Oh that really encourages me to waste more time filling out the form to try and help catch a criminal. At least spring it on me at the end of me completing the form! Regardless, I completed the form. I got a crime number but I don't expect anything to become of it.

The annoying thing is that there is probably CCTV evidence of the culprit in the Vodafone store, and a copy of the fake photo ID that was used. Isn't that solid evidence and enough to get a prosecution? I'd have thought so. I thought it was worth 5 hours of my time chasing around reporting things to different people, but other parties obviously don't.

OK, maybe I wouldn't recognise the person on the CCTV or photo ID, but isn't it worth a shot? And if not, why can't Vodafone or the Police post the picture online? It may only be small fish, but if you get a positive ID from someone, arrest the individual and maybe it'll put them on the right path or just simply prevent them from doing it again in the future.

By not doing anything, you encourage them to steal more, and encourage me less to report anything in the future - to the Police or companies involved. It's completely backwards and not a good outlook for the future of peoples confidence in reporting crime.  