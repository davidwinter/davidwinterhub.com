---
layout: post
title: Poor mans VPN with sshuttle
description: Get around those evil firewalls and secure your net connection
---

It was far too hot in the office the other week and I needed some place a little cooler to work so I could keep focused. I sought refuge at the [Google Campus](http://www.campuslondon.com/) in London which has glorious a/c and a--surprise surprise--super fast internet connection.

However, being in a basement full of geeks, using Google's wifi, and not encrypting your connection, is quite a risky thing to do. I get paranoid about people sniffing my traffic.

I'm a subscriber to [Private Internet Access](https://www.privateinternetaccess.com/), which is a VPN provider. I use that with OpenVPN using [Tunnelblick](https://code.google.com/p/tunnelblick/) for OS X. I've been using it fine for ages and have had no issues. Google doesn't appear to like me encrypting my traffic. I'm able to connect to PIA, but after a while, my wifi connection drops. And then I'm not able to reconnect to Wifi unless I restart my Mac. I've never had this issue before or anywhere else.

Setting up an SSH SOCKS proxy has always been quite reliable and easy way to setup an encrypted connection. I've used it for bypassing my old University's firewall so I could [connect to IRC](/articles/2006/02/06/using-irc-and-other-im-apps-at-university). Also, for getting around [annoying image compression](/articles/2010/06/19/using-o2-payg-mobile-broadband) on my old 3G connection from o2. Though recently I've noticed not all traffic is guaranteed to go over the SOCKS proxy, so it's not as secure as using a VPN.

After lunch, I moved onto [The Book Club](http://www.wearetbc.com/). The wifi was no way near as fast, but the food in there is amazing! They also had an annoying Firewall setup. I couldn't connect to PIA via the VPN either. I setup the SOCKS proxy again but because not all of the traffic goes over the proxy, apps such as Spotify, Hipchat and some others just didn't want to work. Not having some sort of encryption wasn't an option.

With a bit of Googling, I come across [sshuttle](https://github.com/apenwarr/sshuttle). It's written in Python and does some fancy SSH'ing, and allows you to route all traffic over it, without the need of setting up an SSH SOCKS proxy. 

	brew install sshuttle

You just run the command, and it just works. It's amazingly simple:

	sshuttle --dns -vvr david@my.remote.server.com 0/0

Just replace `david@my.remote.server.com` with your remote server that you can SSH into. `sshuttle` will handle the rest. When you're done with the connection, just `CTRL` + `c`.