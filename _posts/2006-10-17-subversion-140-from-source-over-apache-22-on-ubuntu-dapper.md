---
layout: post
title: 'Subversion 1.4.0 from source via Apache 2.2 on Ubuntu Dapper'
tags:
  - apache
  - dapper
  - howto
  - server
  - source
  - subversion
  - ubuntu

---

This howto assumes you've already followed my [Building Apache 2.2 from source](http://davidwinter.me.uk/articles/2006/10/17/building-apache-22-from-source-for-ubuntu-dapper/)  article.

Now we're going to install Subversion 1.4.0 from source so that it can be access via Apache with authentication.

### Installing Subversion

As we've built Apache from source, we'll need to do the same for Subversion in order to get the Apache 2 modules `mod_dav_svn` and `mod_authz_svn`.

	wget http://subversion.tigris.org/downloads/subversion-1.4.0.tar.gz
	tar xvfz subversion-1.4.0.tar.gz
	cd subversion-1.4.0/
	./configure --prefix=/usr/local --with-apxs=/usr/local/apache2/bin/apxs
	make
	sudo make install
	
This will also add the relevant `LoadModule` directives into your Apache 2 configuration for you.

### Creating your repository
	
Now, create your Subversion repository:

	svnadmin create /home/yourusername/subversion/repos

We have to make that repository owned by Apache so that it can be accessed via the web:

	sudo chown -R apache /home/yourusername/subversion/repos

### Authentication File

Now create a user/password file for authentication:

	htpasswd -cm /home/yourusername/subversion/dav_svn.passwd davidwinter
	
When prompted, enter your password.

### Configuring Apache

Edit your `/usr/local/apache2/conf/httpd.conf` file with the following placed at the end:

	<Location /svn>
	  DAV svn
	  SVNPath /home/yourusername/subversion/repos

	  AuthType Basic
	  AuthName "Subversion Repository"
	  AuthUserFile /home/yourusername/subversion/dav_svn.passwd
	  Require valid-user
	</Location>
	
If you want access control based on different users, add the following line after the `Require valid-user` line:

	AuthzSVNAccessFile /home/yourusername/subversion/svn_access_control
	
Save the file. Start Apache. To find out what you need to put in the `svn_access_control` file, read [my previous Subversion authentication article here](http://davidwinter.me.uk/articles/2006/03/03/access-control-for-subversion-with-apache2-and-authz/). for params.
