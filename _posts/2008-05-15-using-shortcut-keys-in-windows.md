---
layout: post
title: 'Using shortcut keys in Windows'
tags:
  - windows

---

You won't find me writing about Windows too much on here I promise. I've been forced to use Windows XP on my arcade machine due to the lack of a decent Sega Mega Drive (Genesis for anyone in the US) emulator on Linux. That has caused other problems though, because the [Fusion emulator](http://www.emulator-zone.com/doc.php/genesis/fusion.html) doesn't allow me to specify a keyboard combination to quit the program via the keyboard. You may be wondering, 'Why not just hit <kbd>Alt + F4</kbd>?' Because I'm hoping not to have a keyboard plugged in all the time, and just use the joystick and buttons (which are mapped to specific keyboard keys).

I was going to try and be clever and modify my Python program to intercept a global hot key combination, and then kill the emulator, but thankfully stumbled across the idea of using a Windows shortcut with a shortcut key (the terminology gets quite confusing). The shortcut would be linked to a batch file that would quit the program.

To cut a long story short (which involved Googling... a lot), shortcut keys can only be used when the shortcut is [located in either the Start menu or on the Desktop](http://support.microsoft.com/kb/134552/en-us).

When I launch the shortcut when the emulator isn't running, it opens instantly. I've noticed however, roughly a 15 second delay for the shortcut to invoke if the emulator is running. I'm guessing that this may be something to do with the event buffer in Windows, and the emulator taking up a chunk of the CPU. Though I'm just guessing. If anyone knows a fix for that, I'd love to hear from you.
