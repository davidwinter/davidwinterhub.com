---
layout: post
title: 'Install APF on Ubuntu 11.04'
tags:
  - apf
  - security
  - ubuntu

---

I just setup a new Linode to host my new twitter app, [lists dj](http://listsdj.com) and wanted to lock it down to basically Apache and SSH.

`iptables` gives me a headache, and I don't want to spend a day learning how to use it. APF uses iptables, but the configuration is so much easier.

    sudo aptitude install apf-firewall
    sudo nano /etc/apf-firewall/conf.apf

If you're using Linode, you'll need to ensure the following line and configuration value is set:

    SET_MONOKERN="1"

This is because `iptables` is installed into the kernel rather than as a package. Not setting this will prevent APF from running.

Then, to open the desired ports, just update this line with the port numbers, separated with a space:

    IG_TCP_CPORTS="22 80 443"

And ensure this line is set:

    DEVEL_MODE="0"

This means that the firewall isn't in development mode, otherwise your rules will be flushed every 5 minutes. Handy if you're experimenting with new rules and don't want to be locked out of your server. Worse case, you'd only have to wait 5 minutes before being able to get back in. But because I'm only opening ports, I don't need this. Just make sure you leave your SSH port open!

Lastly, open up this file:

    sudo nano /etc/default/apf-firewall

And set it to:

    RUN="yes"

Now you can start up APF and you're all protected:

    sudo /etc/init.d/apf-firewall start
