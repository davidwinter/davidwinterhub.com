---
layout: post
title: 'Tomcat on Mac OS X Tiger'
tags:
  - howto
  - java
  - mac
  - tomcat

---

Download the 'Core' .tar.gz version of Tomcat (as of this writing 5.5.15) from http://tomcat.apache.org/download-55.cgi. Extract and move the folder: 

    sudo mv ~/Desktop/apache-tomcat-5.5.15 /usr/local/

That's all you need to do to actually install Tomcat--nice and simple. Now to create some start and stop scripts... 

    cd /usr/local/bin
    sudo nano start_tomcat

Enter the following and save:

    #!/bin/sh
    export CATALINA_HOME=/usr/local/apache-tomcat-5.5.15
    export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Home
    $CATALINA_HOME/bin/startup.sh

Now a stop script:

    sudo nano stop_tomcat

Enter the following:

    #!/bin/sh
    export CATALINA_HOME=/usr/local/apache-tomcat-5.5.15
    export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Home
    $CATALINA_HOME/bin/shutdown.sh

Voila! Now to start Tomcat, use `/usr/local/bin/start_tomcat` and to stop it use `/usr/local/bin/stop_tomcat`.

If you have `/usr/local/bin` included in your PATH environment variable, then you can start and stop Tomcat using `start_tomcat` and `stop_tomcat` respectively straight from the Terminal.

When Tomcat is started, you can see the installation by going to [http://localhost:8080/](http://localhost:8080/). In order to manage Tomcat, you'll need to modify the `tomcat-users.xml` file.

    cd /usr/local/apache-tomcat-5.5.15/conf
    sudo nano tomcat-users.xml

Inside this file, modify it so it includes the following tags:

    <?xml version='1.0' encoding='utf-8'?>
    <tomcat-users>
      <role rolename="manager"/>
      <role rolename="admin"/>
      <user username="davidwinter" password="123" roles="admin,manager"/>
    </tomcat-users>

Where `davidwinter` is the username you want to use, and `123` being the password you desire.

You can then manage your Tomcat applications using this link; [http://localhost:8080/manager/html](http://localhost:8080/manager/html).
