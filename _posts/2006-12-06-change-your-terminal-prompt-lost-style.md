---
layout: post
title: 'Change your Terminal prompt - Lost style'
tags:
  - command-line
  - lost
  - mac
  - unix

---

Fancy a change from the default prompt you get in your Terminal? Want to mimic the Terminal prompt used in [Lost](http://abc.go.com/primetime/lost/index)? I did, and during a boring lecture at Uni, decided to update it.

![Lost Terminal Prompt](http://davidwinter.me.uk/wp-content/uploads/2006/12/lost-prompt.png)

I'm using `zsh` currently, but this will also work for a `bash` shell (default with Mac OS X). I'm using [TextMate](http://macromates.com/) to edit my files, but if you're on Linux ([Ubuntu](http://www.ubuntu.com), right?), then just substitute the `mate` command for `nano` or your favourite editor.

Just to make sure that you're in your home directory:

    cd ~

Now open your shell profile. If you're using `zsh`:

    mate .zprofile

Or if you're using `bash`:

    mate .bashrc

Now add the following line:

    export PS1=">: "

Save and close the file.

Now, make sure you have the right colour scheme going on.

1. 'Terminal' from the main menu.
2. Select 'Window Settings'
3. Select 'Color' in the drop down
4. Select 'Green on Black' from the 'Standard Color Selections'
5. Click the big 'Use Settings as Defaults'
6. Close the window, and you're done

Open a new Terminal window and you should get a nice Lost 'style' for your prompt. Nothing too fancy--just something nice and simple. 

![Lost style Mac prompt](http://davidwinter.me.uk/wp-content/uploads/2006/12/mac-lost-prompt.png)

Now, you only have 108 minutes to enter the code... so don't waste time!

Damn. I can't wait until the next episode.
